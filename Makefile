build:
	docker-compose build

start: stop
	docker-compose up --abort-on-container-exit database

stop:
	docker-compose down --remove-orphans

daemon:
	docker-compose up -d database

build_test:
	docker build -t test_database database/tests

test: stop
	docker-compose run --rm test_database

lint: stop
	docker-compose run --rm test_database bash -c "python -m flake8 ./database ./tests"

pre-push: stop test lint

push: pre-push build
	docker push registry.gitlab.com/maralafris/cashpy/database
	docker push registry.gitlab.com/maralafris/cashpy/test_database

db/connect:
	docker exec -it flaskapistarterkit_db_1 psql -Upostgres

db/downgrade:
	docker-compose run --rm database python database/manage.py db downgrade

db/upgrade:
	docker-compose run --rm database python database/manage.py db upgrade

db/migrate:
	docker-compose run --rm database python database/manage.py db migrate
