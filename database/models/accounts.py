#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
from . import db
from .base import BaseModel
from .banks import BankModel


class AccountModel(db.Model, BaseModel):
    """ Account model """
    id = db.Column(db.Integer, primary_key=True)
    bank_id = db.Column(db.Integer, db.ForeignKey('bank_model.id'))
    bank = db.relationship(BankModel)
    contact = db.Column(db.String)
    comment = db.Column(db.String)
    highlighted = db.Column(db.Boolean, nullable=False)
    name = db.Column(db.String, nullable=False)
    number = db.Column(db.String)
    opened = db.Column(db.Boolean, nullable=False)
    typ = db.Column(db.String, nullable=False)

    def __init__(self, **kwargs):
        """ Create a new Account """
        self.bank_id = kwargs.get('bank_id', None)
        self.contact = kwargs.get('contact', None)
        self.comment = kwargs.get('comment', None)
        self.highlighted = kwargs.get('highlighted', False)
        self.name = kwargs.get('name', None)
        self.number = kwargs.get('number', None)
        self.opened = kwargs.get('opened', False)
        self.typ = kwargs.get('typ', None)

    def __repr__(self):
        return '<Account {}: name:{}, number:{}, type:{}, opened: {}>'.format(
            self.id, self.name, self.number, self.typ, self.opened)
