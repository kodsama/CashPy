#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
from . import db
from .base import BaseModel
from datetime import datetime


class InfoModel(db.Model, BaseModel):
    """ Info model """
    created = db.Column(db.DateTime, default=datetime,
                        primary_key=True, nullable=False)
    updated = db.Column(db.DateTime, default=datetime,
                        onupdate=datetime, nullable=False)
    decimals = db.Column(db.Integer, nullable=False)
    default_unit = db.Column(db.Integer, nullable=False)
    version = db.Column(db.String, nullable=False)

    def __init__(self, **kwargs):
        """ Create a new Info """
        self.created = datetime.now()
        self.updated = datetime.now()
        self.decimals = kwargs.get('decimals', 6)
        self.default_unit = kwargs.get('default_unit', 1)
        self.version = kwargs.get('version', None)

    def __repr__(self):
        return '<Info: version:{} created:{}, updated:{}, \
                decimals:{}, default unit:{} >'.format(
            self.version, self.created, self.updated,
            self.decimals, self.default_unit)
