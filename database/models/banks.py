#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
from . import db
from .base import BaseModel


class BankModel(db.Model, BaseModel):
    """ The bank model """
    id = db.Column(db.Integer, primary_key=True)
    icon = db.Column(db.String)
    name = db.Column(db.String, nullable=False)
    number = db.Column(db.String)

    def __init__(self, **kwargs):
        """ Create a new Bank """
        self.name = kwargs.get('name', None)
        self.number = kwargs.get('number', None)
        self.icon = kwargs.get('icon', None)

    def __repr__(self):
        return '<Bank {}: name:{}, number:{}, icon:{}>'.format(
            self.id, self.name, self.number, self.icon)
