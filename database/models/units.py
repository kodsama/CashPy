#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
from . import db
from .base import BaseModel


class UnitModel(db.Model, BaseModel):
    """ Unit model """
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    country = db.Column(db.String)
    internet_code = db.Column(db.String)
    parent_id = db.Column(db.Integer, db.ForeignKey('unit_model.id'))
    parent = db.relationship('UnitModel',
                             remote_side=[id], post_update=True)
    name = db.Column(db.String, nullable=False)
    source = db.Column(db.String)
    symbol = db.Column(db.String, nullable=False)
    typ = db.Column(db.String)

    def __init__(self, **kwargs):
        """ Create a new Unit """
        self.country = kwargs.get('country', None)
        self.internet_code = kwargs.get('internet_code', None)
        self.parent_id = kwargs.get('parent_id', None)
        self.name = kwargs.get('name', None)
        self.source = kwargs.get('source', None)
        self.symbol = kwargs.get('symbol', None)
        self.typ = kwargs.get('typ', None)

    def __repr__(self):
        return '<Unit {}: name:{}, symbol:{}, country:{}, typ:{}, \
                parent:{}, source:{}, code:{}>'.format(
            self.id, self.name, self.symbol, self.country, self.typ,
            self.parent_id, self.source, self.internet_code)
