#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
from . import db
from .base import BaseModel
from .units import UnitModel


class UnitValueModel(db.Model, BaseModel):
    """ Units values and dates """

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    date = db.Column(db.DateTime, nullable=False)
    unit_id = db.Column(db.Integer, db.ForeignKey('unit_model.id'))
    unit = db.relationship(UnitModel)
    value = db.Column(db.Integer, nullable=False)

    def __init__(self, **kwargs):
        """ Create a new Unit """
        self.date = kwargs.get('date', None)
        self.unit_id = kwargs.get('unit_id', None)
        self.value = kwargs.get('value', None)

    def __repr__(self):
        return '<UnitValue {}: date:{}, value:{}, unit ID:{}>'.format(
            self.id, self.date, self.value, self.unit_id)
