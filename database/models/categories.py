#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
from . import db
from .base import BaseModel


class CategoryModel(db.Model, BaseModel):
    """ Category model """
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String, nullable=False)
    icon = db.Column(db.String)
    parent_id = db.Column(db.Integer, db.ForeignKey('category_model.id'))
    parent = db.relationship('CategoryModel',
                             remote_side=[id], post_update=True)

    def __init__(self, **kwargs):
        """ Create a new Category """
        self.name = kwargs.get('name', None)
        self.icon = kwargs.get('icon', None)
        self.parent_id = kwargs.get('parent_id', None)

    def __repr__(self):
        return '<Category {}: name:{}, icon:{}, parent:{}>'.format(
            self.id, self.name, self.icon, self.parent_id)
