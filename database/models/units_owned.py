#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
from . import db
from .base import BaseModel
from .units import UnitModel


class UnitOwnedModel(db.Model, BaseModel):
    """ Number of units possessed """

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    amount = db.Column(db.Integer, nullable=False)
    date = db.Column(db.DateTime, nullable=False)
    parent_unit_id = db.Column(db.Integer, db.ForeignKey('unit_model.id'))
    parent_unit = db.relationship(UnitModel)

    def __init__(self, **kwargs):
        """ Create a new Unit """
        self.date = kwargs.get('date', None)
        self.parent_unit_id = kwargs.get('parent_unit_id', None)
        self.amount = kwargs.get('amount', None)

    def __repr__(self):
        return '<UnitOwned: date:{}, amont:{}, parent ID:{}>'.format(
            self.date, self.amount, self.parent_unit_id)
