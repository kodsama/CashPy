#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
from . import db
from .base import BaseModel
from .categories import CategoryModel
from .payees import PayeeModel
from .accounts import AccountModel
from datetime import date


class OperationModel(db.Model, BaseModel):
    """ Operation model """

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    account_id = db.Column(db.Integer, db.ForeignKey('account_model.id'))
    account = db.relationship(AccountModel)
    amount = db.Column(db.Integer)
    category_id = db.Column(db.Integer, db.ForeignKey('category_model.id'))
    category = db.relationship(CategoryModel)
    comment = db.Column(db.String)
    date = db.Column(db.Date, nullable=False)
    parent_id = db.Column(db.Integer, db.ForeignKey('operation_model.id'))
    parent = db.relationship('OperationModel',
                             remote_side=[id], post_update=True)
    payee_id = db.Column(db.Integer, db.ForeignKey('payee_model.id'))
    payee = db.relationship(PayeeModel)
    validated = db.Column(db.Boolean, nullable=False)

    def __init__(self, **kwargs):
        """ Create a new Operation """
        self.account_id = kwargs.get('account_id', None)
        self.amount = kwargs.get('amount', None)
        self.category_id = kwargs.get('category_id', None)
        self.comment = kwargs.get('comment', None)
        self.date = kwargs.get('date', date.today())
        self.parent_id = kwargs.get('parent_id', None)
        self.payee_id = kwargs.get('payee_id', None)
        self.validated = kwargs.get('validated', False)

    def __repr__(self):
        return '<Operation {}: account:{}, amount:{}, category: {}, date:{}, \
                 payee: {}, parent:{}, validated:{}, comment"{}>'.format(
            self.id, self.account, self.amount, self.category_id,
            self.date, self.payee_id, self.parent_id,
            self.validated, self.comment)
