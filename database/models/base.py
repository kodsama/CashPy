#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
import logging
from . import db


logger = logging.getLogger(__name__)


class BaseModel():
    """ Generalize __init__, __repr__ and to_json
        Based on the models columns """

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
        except Exception as e:
            logger.error(e)
            self.handle_exception()
            raise
        return self

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
        except Exception as e:
            logger.error(e)
            self.handle_exception()
            raise

    def handle_exception(name):
        db.session.flush()
        logger.debug('Exception handled')

    def to_str(self):
        """ Returns a string of the model content. """
        copy = dict(self)
        for k in copy:
            if k.startswith('_'):
                del(copy[k])
        return '{}:{}'.format(__name__, str(copy))
