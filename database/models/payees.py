#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
from . import db
from .base import BaseModel


class PayeeModel(db.Model, BaseModel):
    """ Payee model """
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    address = db.Column(db.String)
    comment = db.Column(db.String)
    email = db.Column(db.String)
    name = db.Column(db.String, nullable=False)
    phone = db.Column(db.String)
    website = db.Column(db.String)

    def __init__(self, **kwargs):
        """ Create a new Payee """
        self.address = kwargs.get('address', None)
        self.comment = kwargs.get('comment', None)
        self.email = kwargs.get('email', None)
        self.name = kwargs.get('name', None)
        self.phone = kwargs.get('phone', None)
        self.website = kwargs.get('website', None)

    def __repr__(self):
        return '<Payee {}: name:{}, address:{}, \
                comment:{}, email:{}, phone:{}, website: {}>'.format(
            self.id, self.name, self.address, self.comment,
            self.email, self.phone, self.website)
