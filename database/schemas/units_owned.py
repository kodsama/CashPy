#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
from marshmallow import Schema, fields, pre_load, post_load
from datetime import datetime, date


class UnitOwnedSchema(Schema):
    """ Public information about accounts """
    id = fields.Int(primary_key=True, autoincrement=True)
    amount = fields.Int(nullable=False)
    date = fields.DateTime(nullable=False)
    parent_unit_id = fields.Int(nullable=False)

    @pre_load
    @post_load
    def datetime_to_str(self, data):
        """ Workaround because load does not interpret dates correctly """
        if 'date' in data and isinstance(data['date'], (datetime, date)):
            data['date'] = data['date'].strftime('%Y-%m-%dT%H:%M:%S')
