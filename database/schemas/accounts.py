#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
from marshmallow import Schema, fields


class AccountSchema(Schema):
    """ Public information about accounts """
    id = fields.Int(primary_key=True, autoincrement=True)
    bank_id = fields.Int(nullable=False)
    contact = fields.Str()
    comment = fields.Str()
    highlighted = fields.Bool(nullable=False)
    name = fields.Str(nullable=False)
    number = fields.Str()
    opened = fields.Bool(nullable=False)
    typ = fields.Str(nullable=False)
