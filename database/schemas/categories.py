#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
from marshmallow import Schema, fields


class CategorySchema(Schema):
    """ Public information about accounts """
    id = fields.Int(primary_key=True, autoincrement=True)
    name = fields.Str(nullable=False)
    icon = fields.Str()
    parent_id = fields.Int()
