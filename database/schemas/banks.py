#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
from marshmallow import Schema, fields


class BankSchema(Schema):
    """ Public information about banks """
    id = fields.Int(required=True, nullable=False)
    icon = fields.Str()
    name = fields.Str(nullable=False)
    number = fields.Str()
