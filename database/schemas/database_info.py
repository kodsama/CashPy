#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
from marshmallow import Schema, fields


class InfoSchema(Schema):
    """ Public information about info """
    created = fields.DateTime(nullable=False)
    updated = fields.DateTime(nullable=False)
    decimals = fields.Int(nullable=False)
    default_unit = fields.Int(nullable=False)
    version = fields.Str(nullable=False)
