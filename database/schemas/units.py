#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
from marshmallow import Schema, fields


class UnitSchema(Schema):
    """ Public information about units """
    id = fields.Int(primary_key=True, autoincrement=True)
    country = fields.Str()
    internet_code = fields.Str()
    parent_id = fields.Int(nullable=False)
    name = fields.Str(nullable=False)
    source = fields.Str()
    symbol = fields.Str(nullable=False)
    typ = fields.Str()
