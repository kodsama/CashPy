#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
from marshmallow import Schema, fields, pre_load, post_load
from datetime import datetime, date


class OperationSchema(Schema):
    """ Public information about operations """

    id = fields.Int(primary_key=True, autoincrement=True)
    account_id = fields.Int(nullable=False)
    amount = fields.Int(nullable=False)
    category_id = fields.Int(nullable=False)
    comment = fields.String()
    date = fields.Date(nullable=False)
    parent_id = fields.Int()
    payee_id = fields.Int(nullable=False)
    validated = fields.Bool(nullable=False)

    @pre_load
    @post_load
    def date_to_str(self, data):
        """ Workaround because load does not interpret dates correctly """
        if 'date' in data and isinstance(data['date'], (datetime, date)):
            data['date'] = data['date'].strftime('%Y-%m-%d')
