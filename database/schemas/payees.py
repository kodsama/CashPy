#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
from marshmallow import Schema, fields


class PayeeSchema(Schema):
    """ Public information about payees """
    id = fields.Int(primary_key=True, autoincrement=True)
    address = fields.Str()
    comment = fields.Str()
    email = fields.Str()
    name = fields.Str(nullable=False)
    phone = fields.Str()
    website = fields.Str()
