#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
import logging
import os
import sys


DEBUG = True if os.getenv('DEBUG', 0) in ['true', 'True', 1] else False
APPLICATION_ROOT = os.getenv('APPLICATION_APPLICATION_ROOT')
HOST = os.getenv('APPLICATION_HOST', '0.0.0.0')
if HOST == '0.0.0.0' and len(sys.argv) > 1:
    HOST = sys.argv[1]
PORT = int(os.getenv('APPLICATION_PORT', '3000'))

DB_CONTAINER = os.getenv('APPLICATION_DB_CONTAINER', 'db')
POSTGRES = {
    'user': os.getenv('APPLICATION_POSTGRES_USER', 'postgres'),
    'pw': os.getenv('APPLICATION_POSTGRES_PW', ''),
    'host': os.getenv('APPLICATION_POSTGRES_HOST', DB_CONTAINER),
    'port': os.getenv('APPLICATION_POSTGRES_PORT', 5432),
    'db': os.getenv('APPLICATION_POSTGRES_DB', 'postgres'),
}
DB_URI = 'postgresql://%(user)s:%(pw)s@%(host)s:%(port)s/%(db)s' % POSTGRES

logging.basicConfig(
    filename=os.getenv('SERVICE_LOG', 'server.log'),
    level=logging.DEBUG,
    format='%(levelname)s: %(asctime)s \
        pid:%(process)s module:%(module)s %(message)s',
    datefmt='%d/%m/%y %H:%M:%S',
)
