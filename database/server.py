#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
import time
import logging

from flask import Flask
from flask import request
from flask.blueprints import Blueprint
from flask.logging import default_handler
from flasgger import Swagger
from sqlalchemy.exc import OperationalError

import config
from models import db
import routes


class DatabaseServer():
    ''' Configure the flask server and database link '''
    def __init__(self):
        self.server = None
        self.server_host = config.HOST
        self.server_port = config.PORT
        self.server_debug = config.DEBUG
        self.logger = self.logging_init()

        self.db = db
        self.db_uri = config.DB_URI
        self.db_connected = False
        self.running = False

        self.server_init()
        self.connect()
        self.config_blueprints()

    def logging_init(self):
        ''' Sets up logging '''
        logger = logging.getLogger()
        FORMAT = "[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s"
        logging.basicConfig(format=FORMAT)
        if self.server_debug:
            logger.setLevel(logging.DEBUG)
        else:
            logger.setLevel(logging.ERROR)
        logger.addHandler(default_handler)
        return logger

    def server_init(self):
        ''' Setup and configure the flask server '''
        self.server = Flask(__name__)

        self.server.config['SWAGGER'] = {
            "swagger_version": "2.0",
            "title": "CashPy",
            "specs": [
                {
                    "version": "0.0.1",
                    "title": "CashPy",
                    "endpoint": 'spec',
                    "route": '/spec',
                    "rule_filter": lambda rule: True  # all in
                }
            ],
            "static_url_path": "/apidocs"
        }

        Swagger(self.server)

        self.server.debug = self.server_debug
        self.server.config['SQLALCHEMY_DATABASE_URI'] = self.db_uri
        self.server.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
        self.db.init_app(self.server)
        self.db.app = self.server

    def connect(self, sleep=1, attempt=10):
        ''' Connect to the database '''
        for i in range(1, attempt + 1):
            try:
                self.db.create_all()
            except OperationalError:
                self.logger.warwning('DB connect to %s FAIL (try %d).',
                                     self.db_uri, i)
                time.sleep(sleep)
            else:
                self.db_connected = True
                break
        if self.db_connected:
            self.logger.info('DB connected: %s', self.db_uri)
            return
        self.logger.error('DB %s NOT CONNECTED (%d tries): %s', self.db_uri, i)

    def config_blueprints(self):
        ''' Setup blueprints '''
        for blueprint in vars(routes).values():
            if isinstance(blueprint, Blueprint):
                self.logger.debug('Add blueprint: %s', blueprint.name)
                self.server.register_blueprint(
                    blueprint,
                    url_prefix=config.APPLICATION_ROOT
                )

    def run(self):
        ''' Run the flask server '''
        self.running = True
        try:
            self.server.run(host=self.server_host, port=self.server_port)
        except Exception as e:
            self.logger.error(e)
            self.running = False
            raise e

    def stop(self):
        try:
            func = request.environ.get('werkzeug.server.shutdown')
        except RuntimeError as e:
            self.logger.debug('Got runtime error while stopping: %s', e)
            pass    # No server running
        else:
            if func is not None:
                func()
        self.running = False


if __name__ == '__main__':
    db_server = DatabaseServer()    # pragma: no cover
    db_server.run()                 # pragma: no cover
