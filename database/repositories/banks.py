#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
import logging

from models import BankModel
from schemas import BankSchema


logger = logging.getLogger(__name__)


class BankRepository:
    """ The repository for the bank model """

    @staticmethod
    def get(id):
        """ Query a bank by ID """
        bank = BankModel.query.filter_by(id=id).first()
        logger.debug('Get ID %d: got:%s', id, bank)
        return bank

    @staticmethod
    def get_all():
        """ Query all banks """
        banks = BankModel.query.all()
        logger.debug('Get all, got:%s', banks)
        return banks

    def update(self, **kwargs):
        """ Update the bank """
        data = BankSchema().load(kwargs).data
        bank = self.get(data['id'])
        logger.debug('update ID %d: got:%s', data['id'], bank)
        if not bank:
            return None
        for k in data:
            if data[k]:
                setattr(bank, k, data[k])
        logger.debug('update ID %d: now:%s', data['id'], bank)
        return bank.save()

    @staticmethod
    def create(**kwargs):
        """ Create a bank """
        data = BankSchema().load(kwargs).data
        logger.debug('create: %s', data)
        bank = BankModel(**data)
        return bank.save()

    def delete(self, id):
        """ Delete an bank based on the sent id """
        bank = self.get(id)
        logger.debug('delete ID %d: was:%s', id, bank)
        if not bank:
            return None
        old_bank = bank
        bank.delete()
        return old_bank
