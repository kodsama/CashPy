#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
import logging

from models import OperationModel
from schemas import OperationSchema


logger = logging.getLogger(__name__)


class OperationRepository:
    """ The repository for the operation model """

    @staticmethod
    def get(id):
        """ Query an operation by ID """
        operation = OperationModel.query.filter_by(id=id).first()
        logger.debug('Get ID %d: got:%s', id, operation)
        return operation

    @staticmethod
    def get_all():
        """ Query all operations """
        operations = OperationModel.query.all()
        logger.debug('Get all, got:%s', operations)
        return operations

    def update(self, **kwargs):
        """ Update an operation """
        data = OperationSchema().load(kwargs).data
        operation = self.get(data['id'])
        logger.debug('update ID %d: got:%s', data['id'], operation)
        if not operation:
            return None
        for k in data:
            if data[k]:
                setattr(operation, k, data[k])
        logger.debug('update ID %d: now:%s', data['id'], operation)
        return operation.save()

    @staticmethod
    def create(**kwargs):
        """ Create an operation """
        data = OperationSchema().load(kwargs).data
        logger.debug('create: %s', data)
        operation = OperationModel(**data)
        return operation.save()

    def delete(self, id):
        """ Delete an operation based on the sent id """
        operation = self.get(id)
        logger.debug('delete ID %d: was:%s', id, operation)
        if not operation:
            return None
        old_operation = operation
        operation.delete()
        return old_operation
