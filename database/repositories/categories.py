#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
import logging

from models import CategoryModel
from schemas import CategorySchema


logger = logging.getLogger(__name__)


class CategoryRepository:
    """ The repository for the category model """

    @staticmethod
    def get(id):
        """ Query a category by ID """
        category = CategoryModel.query.filter_by(id=id).first()
        logger.debug('Get ID %d: got:%s', id, category)
        return category

    @staticmethod
    def get_all():
        """ Query all categories """
        categories = CategoryModel.query.all()
        logger.debug('Get all, got:%s', categories)
        return categories

    def update(self, **kwargs):
        """ Update a category """
        data = CategorySchema().load(kwargs).data
        category = self.get(data['id'])
        logger.debug('update ID %d: got:%s', data['id'], category)
        if not category:
            return None
        for k in data:
            if data[k]:
                setattr(category, k, data[k])
        logger.debug('update ID %d: now:%s', data['id'], category)
        return category.save()

    @staticmethod
    def create(**kwargs):
        """ Create a category """
        data = CategorySchema().load(kwargs).data
        logger.debug('create: %s', data)
        category = CategoryModel(**data)
        return category.save()

    def delete(self, id):
        """ Delete a category based on the sent id """
        category = self.get(id)
        logger.debug('delete ID %d: was:%s', id, category)
        if not category:
            return None
        old_category = category
        category.delete()
        return old_category
