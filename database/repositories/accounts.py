#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
import logging

from models import AccountModel
from schemas import AccountSchema


logger = logging.getLogger(__name__)


class AccountRepository:
    """ The repository for the account model """

    @staticmethod
    def get(id):
        """ Query an account by ID """
        account = AccountModel.query.filter_by(id=id).first()
        logger.debug('Get ID %d: got:%s', id, account)
        return account

    @staticmethod
    def get_all():
        """ Query all accounts """
        accounts = AccountModel.query.all()
        logger.debug('Get all, got:%s', accounts)
        return accounts

    def update(self, **kwargs):
        """ Update an account """
        data = AccountSchema().load(kwargs).data
        account = self.get(data['id'])
        logger.debug('update ID %d: got:%s', data['id'], account)
        if not account:
            return None
        for k in data:
            if data[k]:
                setattr(account, k, data[k])
        logger.debug('update ID %d: now:%s', data['id'], account)
        return account.save()

    @staticmethod
    def create(**kwargs):
        """ Create an account """
        data = AccountSchema().load(kwargs).data
        logger.debug('create: %s', data)
        account = AccountModel(**data)
        return account.save()

    def delete(self, id):
        """ Delete an account based on the sent id """
        account = self.get(id)
        logger.debug('delete ID %d: was:%s', id, account)
        if not account:
            return None
        old_account = account
        account.delete()
        return old_account
