#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
import logging

from models import InfoModel
from schemas import InfoSchema
from datetime import datetime


logger = logging.getLogger(__name__)


class InfoRepository:
    """ The repository for the info model. Only 1 entry is allowed """

    @staticmethod
    def get():
        """ Query info """
        info = InfoModel.query.first()
        logger.debug('get: got:%s', info)
        return info

    def update(self, **kwargs):
        """ Update an info """
        data = InfoSchema().load(kwargs).data
        info = InfoModel.query.all()
        logger.debug('update: got:%s', info)
        if not info:
            return None
        info = info[0]
        for k in data:
            if data[k]:
                setattr(info, k, data[k])
        logger.debug('update: now:%s', info)
        info.updated = datetime.now()
        return info.save()

    @staticmethod
    def create(**kwargs):
        """ Create an info """
        data = InfoSchema().load(kwargs).data
        info = InfoModel.query.all()
        if info:
            logger.warning('create: Entry already exist: %s', info)
            raise ValueError('Tried to create a second entry, not allowed')
        logger.debug('create: %s', data)
        info = InfoModel(**data)
        return info.save()

    def delete(self):
        """ Delete an info """
        info = InfoModel.query.all()
        logger.debug('delete info: was:%s', info)
        if not info:
            return None
        if len(info) > 1:
            raise ValueError('More than 1 Info is registered: %s', info)
        info = info[0]
        old_info = info
        info.delete()
        return old_info
