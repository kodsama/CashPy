#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
import logging

from models import UnitValueModel
from schemas import UnitValueSchema


logger = logging.getLogger(__name__)


class UnitValueRepository:
    """ The repository for the unit value model """

    @staticmethod
    def get(id):
        """ Query an unit value by ID """
        unit_value = UnitValueModel.query.filter_by(id=id).first()
        logger.debug('Get ID %d: got:%s', id, unit_value)
        return unit_value

    @staticmethod
    def get_all():
        """ Query all unit values """
        unit_values = UnitValueModel.query.all()
        logger.debug('Get all, got:%s', unit_values)
        return unit_values

    def update(self, **kwargs):
        """ Update an unit value """
        data = UnitValueSchema().load(kwargs).data
        unit_value = self.get(data['id'])
        logger.debug('update ID %d: got:%s', data['id'], unit_value)
        if not unit_value:
            return None
        for k in data:
            if data[k]:
                setattr(unit_value, k, data[k])
        logger.debug('update ID %d: now:%s', data['id'], unit_value)
        return unit_value.save()

    @staticmethod
    def create(**kwargs):
        """ Create an unit value """
        data = UnitValueSchema().load(kwargs).data
        logger.debug('create: %s', data)
        unit_value = UnitValueModel(**data)
        return unit_value.save()

    def delete(self, id):
        """ Delete an unit value based on the sent date """
        unit_value = self.get(id)
        logger.debug('delete ID %d: was:%s', id, unit_value)
        if not unit_value:
            return None
        old_unit_value = unit_value
        unit_value.delete()
        return old_unit_value
