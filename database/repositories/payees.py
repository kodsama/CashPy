#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
import logging

from models import PayeeModel
from schemas import PayeeSchema


logger = logging.getLogger(__name__)


class PayeeRepository:
    """ The repository for the payee model """

    @staticmethod
    def get(id):
        """ Query an payee by ID """
        payee = PayeeModel.query.filter_by(id=id).first()
        logger.debug('Get ID %d: got:%s', id, payee)
        return payee

    @staticmethod
    def get_all():
        """ Query all payees """
        payees = PayeeModel.query.all()
        logger.debug('Get all, got:%s', payees)
        return payees

    def update(self, **kwargs):
        """ Update an payee """
        data = PayeeSchema().load(kwargs).data
        payee = self.get(data['id'])
        logger.debug('update ID %d: got:%s', data['id'], payee)
        if not payee:
            return None
        for k in data:
            if data[k]:
                setattr(payee, k, data[k])
        logger.debug('update ID %d: now:%s', data['id'], payee)
        return payee.save()

    @staticmethod
    def create(**kwargs):
        """ Create an payee """
        data = PayeeSchema().load(kwargs).data
        logger.debug('create: %s', data)
        payee = PayeeModel(**data)
        return payee.save()

    def delete(self, id):
        """ Delete an payee based on the sent id """
        payee = self.get(id)
        logger.debug('delete ID %d: was:%s', id, payee)
        if not payee:
            return None
        old_payee = payee
        payee.delete()
        return old_payee
