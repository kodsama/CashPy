#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
import logging

from models import UnitModel
from schemas import UnitSchema


logger = logging.getLogger(__name__)


class UnitRepository:
    """ The repository for the unit model """

    @staticmethod
    def get(id):
        """ Query an unit by ID """
        unit = UnitModel.query.filter_by(id=id).first()
        logger.debug('Get ID %d: got:%s', id, unit)
        return unit

    @staticmethod
    def get_all():
        """ Query all units """
        units = UnitModel.query.all()
        logger.debug('Get all, got:%s', units)
        return units

    def update(self, **kwargs):
        """ Update an unit """
        data = UnitSchema().load(kwargs).data
        unit = self.get(data['id'])
        logger.debug('update ID %d: got:%s', data['id'], unit)
        if not unit:
            return None
        for k in data:
            if data[k]:
                setattr(unit, k, data[k])
        logger.debug('update ID %d: now:%s', data['id'], unit)
        return unit.save()

    @staticmethod
    def create(**kwargs):
        """ Create an unit """
        data = UnitSchema().load(kwargs).data
        logger.debug('create: %s', data)
        unit = UnitModel(**data)
        return unit.save()

    def delete(self, id):
        """ Delete an unit based on the sent id """
        unit = self.get(id)
        logger.debug('delete ID %d: was:%s', id, unit)
        if not unit:
            return None
        old_unit = unit
        unit.delete()
        return old_unit
