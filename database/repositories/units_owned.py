#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
import logging

from models import UnitOwnedModel
from schemas import UnitOwnedSchema


logger = logging.getLogger(__name__)


class UnitOwnedRepository:
    """ The repository for the unit owned model """

    @staticmethod
    def get(id):
        """ Query an unit owned by ID """
        unit_owned = UnitOwnedModel.query.filter_by(id=id).first()
        logger.debug('Get ID %d: got:%s', id, unit_owned)
        return unit_owned

    @staticmethod
    def get_all():
        """ Query all unit owneds """
        unit_owneds = UnitOwnedModel.query.all()
        logger.debug('Get all, got:%s', unit_owneds)
        return unit_owneds

    def update(self, **kwargs):
        """ Update an unit_owned """
        data = UnitOwnedSchema().load(kwargs).data
        unit_owned = self.get(data['id'])
        logger.debug('update ID %d: got:%s', data['id'], unit_owned)
        if not unit_owned:
            return None
        for k in data:
            if data[k]:
                setattr(unit_owned, k, data[k])
        logger.debug('update ID %d: now:%s', data['id'], unit_owned)
        return unit_owned.save()

    @staticmethod
    def create(**kwargs):
        """ Create an unit owned """
        data = UnitOwnedSchema().load(kwargs).data
        logger.debug('create: %s', data)
        unit_owned = UnitOwnedModel(**data)
        return unit_owned.save()

    def delete(self, id):
        """ Delete an unit owned based on the sent id """
        unit_owned = self.get(id)
        logger.debug('delete ID %d: was:%s', id, unit_owned)
        if not unit_owned:
            return None
        old_unit_owned = unit_owned
        unit_owned.delete()
        return old_unit_owned
