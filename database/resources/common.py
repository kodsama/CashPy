#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
from functools import wraps
from flask_restful import reqparse


def parse_params(*arguments):
    """
    Parse the parameters
    Forward them to the wrapped function as named parameters
    """
    def parse(func):
        """ Wrapper """
        @wraps(func)
        def resource_verb(*args, **kwargs):
            """ Decorated function """
            parser = reqparse.RequestParser()
            for argument in arguments:
                parser.add_argument(argument)
            kwargs.update(parser.parse_args())
            return func(*args, **kwargs)
        return resource_verb
    return parse
