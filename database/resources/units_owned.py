#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
import logging
from flasgger import swag_from
from flask_restful import Resource
from flask_restful.reqparse import Argument
from flask import request as req

from .common import parse_params
from schemas import UnitOwnedSchema
from repositories import UnitOwnedRepository


logger = logging.getLogger(__name__)


class UnitOwnedResource(Resource):
    """ Verbs relative to the units_owned """

    @staticmethod
    @swag_from('../swagger/units_owned/GET.yml')
    def get(id):
        """ Return an unit_owned key information based on its id """
        logger.debug('Recv %s:%s from %s', req.url, req.data, req.remote_addr)
        try:
            unit_owned = UnitOwnedRepository.get(id=id)
        except Exception as e:
            return {'error': '{}'.format(e)}, 409
        if unit_owned:
            result = UnitOwnedSchema().dump(unit_owned)
            return {'unit_owned': result}, 200
        return {'message': 'UnitOwned ID {} not found'.format(id)}, 404

    @staticmethod
    @parse_params(Argument('amount', location='json', required=False),)
    @parse_params(Argument('date', location='json', required=False),)
    @parse_params(Argument('parent_unit_id', location='json', required=False),)
    @swag_from('../swagger/units_owned/PUT.yml')
    def put(**kwargs):
        """ Update an unit_owned based on the sent information """
        logger.debug('Recv %s:%s from %s', req.url, req.data, req.remote_addr)
        repository = UnitOwnedRepository()
        data = UnitOwnedSchema().load(kwargs).data
        try:
            unit_owned = repository.update(**data)
        except Exception as e:
            return {'error': '{}'.format(e)}, 409
        if unit_owned:
            result = UnitOwnedSchema().dump(unit_owned)
            return {'message': 'Updated unit_owned', 'unit_owned': result}, 200
        return {'message': 'UnitOwned ID {} not found'.format(data['id'])}, 404

    @staticmethod
    @swag_from('../swagger/units_owned/DEL.yml')
    def delete(id):
        ''' Delete a unit_owned with provided ID '''
        logger.debug('Recv %s:%s from %s', req.url, req.data, req.remote_addr)
        repository = UnitOwnedRepository()
        try:
            unit_owned = repository.delete(id=id)
        except Exception as e:
            return {'error': '{}'.format(e)}, 409
        if unit_owned:
            result = UnitOwnedSchema().dump(unit_owned)
            return {'message': 'Deleted unit_owned', 'unit_owned': result}, 202
        return {'message': 'UnitOwned ID {} not found'.format(id)}, 404


class UnitOwnedListResource(Resource):
    """ Verbs relative to the units_owned """

    @staticmethod
    @swag_from('../swagger/units_owned/GET_all.yml')
    def get():
        """ Return all unit_owned key information """
        logger.debug('Recv %s:%s from %s', req.url, req.data, req.remote_addr)
        try:
            unit_owned = UnitOwnedRepository.get_all()
        except Exception as e:
            return {'error': '{}'.format(e)}, 409
        if unit_owned:
            result = UnitOwnedSchema(many=True).dump(unit_owned)
            return {'unit_owned': result}, 200
        return {'message': 'No units_owned found'}, 404

    @staticmethod
    @parse_params(Argument('amount', location='json', required=True),)
    @parse_params(Argument('date', location='json', required=True),)
    @parse_params(Argument('parent_unit_id', location='json', required=True),)
    @swag_from('../swagger/units_owned/POST.yml')
    def post(**kwargs):
        """ Create an unit_owned based on the sent information """
        logger.debug('Recv %s:%s from %s', req.url, req.data, req.remote_addr)
        data = UnitOwnedSchema().load(kwargs).data
        try:
            unit_owned = UnitOwnedRepository.create(**data)
        except Exception as e:
            return {'error': '{}'.format(e)}, 409
        if unit_owned:
            result = UnitOwnedSchema().dump(unit_owned)
            return {'message': 'Created unit_owned', 'unit_owned': result}, 201
        return {'message': 'Bad request'}, 400  # pragma: no cover
