#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
from flasgger import swag_from
from flask_restful import Resource


class RootResource(Resource):
    """ Verbs relative to the root """

    @staticmethod
    @swag_from('../swagger/root/GET.yml')
    def get():
        return 'Please go to /apidocs or /spec to discover more.', 418

    @swag_from('../swagger/root/POST.yml')
    def post(request):
        return 'Not supported, sorry...', 405

    @swag_from('../swagger/root/PUT.yml')
    def put(request):
        return 'Not supported, sorry...', 405
