#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
import logging
from flasgger import swag_from
from flask_restful import Resource
from flask_restful.reqparse import Argument
from flask import request as req

from .common import parse_params
from schemas import UnitSchema
from repositories import UnitRepository


logger = logging.getLogger(__name__)


class UnitResource(Resource):
    """ Verbs relative to the units """

    @staticmethod
    @swag_from('../swagger/units/GET.yml')
    def get(id):
        """ Return an unit key information based on its id """
        logger.debug('Recv %s:%s from %s', req.url, req.data, req.remote_addr)
        try:
            unit = UnitRepository.get(id=id)
        except Exception as e:
            return {'error': '{}'.format(e)}, 409
        if unit:
            result = UnitSchema().dump(unit)
            return {'unit': result}, 200
        return {'message': 'Unit ID {} not found'.format(id)}, 404

    @staticmethod
    @parse_params(Argument('country', location='json', required=False),)
    @parse_params(Argument('internet_code', location='json', required=False),)
    @parse_params(Argument('parent_id', location='json', required=False),)
    @parse_params(Argument('name', location='json', required=False),)
    @parse_params(Argument('source', location='json', required=False),)
    @parse_params(Argument('symbol', location='json', required=False),)
    @parse_params(Argument('typ', location='json', required=False),)
    @swag_from('../swagger/units/PUT.yml')
    def put(**kwargs):
        """ Update an unit based on the sent information """
        logger.debug('Recv %s:%s from %s', req.url, req.data, req.remote_addr)
        repository = UnitRepository()
        data = UnitSchema().load(kwargs).data
        try:
            unit = repository.update(**data)
        except Exception as e:
            return {'error': '{}'.format(e)}, 409
        if unit:
            result = UnitSchema().dump(unit)
            return {'message': 'Updated unit', 'unit': result}, 200
        return {'message': 'Unit ID {} not found'.format(data['id'])}, 404

    @staticmethod
    @swag_from('../swagger/units/DEL.yml')
    def delete(id):
        ''' Delete a unit with provided ID '''
        logger.debug('Recv %s:%s from %s', req.url, req.data, req.remote_addr)
        repository = UnitRepository()
        try:
            unit = repository.delete(id=id)
        except Exception as e:
            return {'error': '{}'.format(e)}, 409
        if unit:
            result = UnitSchema().dump(unit)
            return {'message': 'Deleted unit', 'unit': result}, 202
        return {'message': 'Unit ID {} not found'.format(id)}, 404


class UnitListResource(Resource):
    """ Verbs relative to the units """

    @staticmethod
    @swag_from('../swagger/units/GET_all.yml')
    def get():
        """ Return all unit key information """
        logger.debug('Recv %s:%s from %s', req.url, req.data, req.remote_addr)
        try:
            unit = UnitRepository.get_all()
        except Exception as e:
            return {'error': '{}'.format(e)}, 409
        if unit:
            result = UnitSchema(many=True).dump(unit)
            return {'unit': result}, 200
        return {'message': 'No units found'}, 404

    @staticmethod
    @parse_params(Argument('country', location='json', required=False),)
    @parse_params(Argument('internet_code', location='json', required=False),)
    @parse_params(Argument('parent_id', location='json', required=False),)
    @parse_params(Argument('name', location='json', required=True),)
    @parse_params(Argument('source', location='json', required=False),)
    @parse_params(Argument('symbol', location='json', required=True),)
    @parse_params(Argument('typ', location='json', required=False),)
    @swag_from('../swagger/units/POST.yml')
    def post(**kwargs):
        """ Create an unit based on the sent information """
        logger.debug('Recv %s:%s from %s', req.url, req.data, req.remote_addr)
        data = UnitSchema().load(kwargs).data
        try:
            unit = UnitRepository.create(**data)
        except Exception as e:
            return {'error': '{}'.format(e)}, 409
        if unit:
            result = UnitSchema().dump(unit)
            return {'message': 'Created unit', 'unit': result}, 201
        return {'message': 'Bad request'}, 400  # pragma: no cover
