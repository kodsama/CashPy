#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
import logging
from flasgger import swag_from
from flask_restful import Resource
from flask_restful.reqparse import Argument
from flask import request as req

from .common import parse_params
from schemas import OperationSchema
from repositories import OperationRepository


logger = logging.getLogger(__name__)


class OperationResource(Resource):
    """ Verbs relative to the operations """

    @staticmethod
    @swag_from('../swagger/operations/GET.yml')
    def get(id):
        """ Return an operation key information based on its id """
        logger.debug('Recv %s:%s from %s', req.url, req.data, req.remote_addr)
        try:
            operation = OperationRepository.get(id=id)
        except Exception as e:
            return {'error': '{}'.format(e)}, 409
        if operation:
            result = OperationSchema().dump(operation)
            return {'operation': result}, 200
        return {'message': 'Operation ID {} not found'.format(id)}, 404

    @staticmethod
    @parse_params(Argument('account_id', location='json', required=False),)
    @parse_params(Argument('amount', location='json', required=False),)
    @parse_params(Argument('category_id', location='json', required=False),)
    @parse_params(Argument('comment', location='json', required=False),)
    @parse_params(Argument('date', location='json', required=False),)
    @parse_params(Argument('parent_id', location='json', required=False),)
    @parse_params(Argument('payee_id', location='json', required=False),)
    @parse_params(Argument('validated', location='json', required=False),)
    @swag_from('../swagger/operations/PUT.yml')
    def put(**kwargs):
        """ Update an operation based on the sent information """
        logger.debug('Recv %s:%s from %s', req.url, req.data, req.remote_addr)
        repository = OperationRepository()
        data = OperationSchema().load(kwargs).data
        try:
            operation = repository.update(**data)
        except Exception as e:
            return {'error': '{}'.format(e)}, 409
        if operation:
            result = OperationSchema().dump(operation)
            return {'message': 'Updated operation', 'operation': result}, 200
        return {'message': 'Operation ID {} not found'.format(data['id'])}, 404

    @staticmethod
    @swag_from('../swagger/operations/DEL.yml')
    def delete(id):
        ''' Delete a operation with provided ID '''
        logger.debug('Recv %s:%s from %s', req.url, req.data, req.remote_addr)
        repository = OperationRepository()
        try:
            operation = repository.delete(id=id)
        except Exception as e:
            return {'error': '{}'.format(e)}, 409
        if operation:
            result = OperationSchema().dump(operation)
            return {'message': 'Deleted operation', 'operation': result}, 202
        return {'message': 'Operation ID {} not found'.format(id)}, 404


class OperationListResource(Resource):
    """ Verbs relative to the operations """

    @staticmethod
    @swag_from('../swagger/operations/GET_all.yml')
    def get():
        """ Return all operation key information """
        logger.debug('Recv %s:%s from %s', req.url, req.data, req.remote_addr)
        try:
            operation = OperationRepository.get_all()
        except Exception as e:
            return {'error': '{}'.format(e)}, 409
        if operation:
            result = OperationSchema(many=True).dump(operation)
            return {'operation': result}, 200
        return {'message': 'No operations found'}, 404

    @staticmethod
    @parse_params(Argument('account_id', location='json', required=True),)
    @parse_params(Argument('amount', location='json', required=True),)
    @parse_params(Argument('category_id', location='json', required=True),)
    @parse_params(Argument('comment', location='json', required=False),)
    @parse_params(Argument('date', location='json', required=True),)
    @parse_params(Argument('parent_id', location='json', required=False),)
    @parse_params(Argument('payee_id', location='json', required=True),)
    @parse_params(Argument('validated', location='json', required=True),)
    @swag_from('../swagger/operations/POST.yml')
    def post(**kwargs):
        """ Create an operation based on the sent information """
        logger.debug('Recv %s:%s from %s', req.url, req.data, req.remote_addr)
        data = OperationSchema().load(kwargs).data
        try:
            operation = OperationRepository.create(**data)
        except Exception as e:
            return {'error': '{}'.format(e)}, 409
        if operation:
            result = OperationSchema().dump(operation)
            return {'message': 'Created operation', 'operation': result}, 201
        return {'message': 'Bad request'}, 400  # pragma: no cover
