#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
import logging
from flasgger import swag_from
from flask_restful import Resource
from flask_restful.reqparse import Argument
from flask import request as req

from .common import parse_params
from schemas import BankSchema
from repositories import BankRepository


logger = logging.getLogger(__name__)


class BankResource(Resource):
    """ Verbs relative to the banks """

    @staticmethod
    @swag_from('../swagger/banks/GET.yml')
    def get(id):
        """ Return a bank key information based on its id """
        logger.debug('Recv %s:%s from %s', req.url, req.data, req.remote_addr)
        bank = BankRepository.get(id=id)
        if bank:
            result = BankSchema().dump(bank)
            return {'bank': result}, 200
        return {'message': 'Bank ID {} not found'.format(id)}, 404

    @staticmethod
    @parse_params(Argument('name', location='json', required=False),)
    @parse_params(Argument('icon', location='json', required=False),)
    @parse_params(Argument('number', location='json', required=False),)
    @swag_from('../swagger/banks/PUT.yml')
    def put(**kwargs):
        """ Update a bank based on the sent information """
        logger.debug('Recv %s:%s from %s', req.url, req.data, req.remote_addr)
        repository = BankRepository()
        data = BankSchema().load(kwargs).data
        bank = repository.update(**data)
        if bank:
            result = BankSchema().dump(bank)
            return {'message': 'Updated bank', 'bank': result}, 200
        return {'message': 'Bank ID {} not found'.format(data['id'])}, 404

    @staticmethod
    @swag_from('../swagger/banks/DEL.yml')
    def delete(id):
        ''' Delete a bank with provided ID '''
        logger.debug('Recv %s:%s from %s', req.url, req.data, req.remote_addr)
        repository = BankRepository()
        bank = repository.delete(id=id)
        if bank:
            result = BankSchema().dump(bank)
            return {'message': 'Deleted bank', 'bank': result}, 202
        return {'message': 'Bank ID {} not found'.format(id)}, 404


class BankListResource(Resource):
    """ Verbs relative to the banks """

    @staticmethod
    @swag_from('../swagger/banks/GET_all.yml')
    def get():
        """ Return all banks key information """
        logger.debug('Recv %s:%s from %s', req.url, req.data, req.remote_addr)
        bank = BankRepository.get_all()
        if bank:
            result = BankSchema(many=True).dump(bank)
            return {'bank': result}, 200
        return {'message': 'No banks found'}, 404

    @staticmethod
    @parse_params(Argument('name', location='json', required=True),)
    @parse_params(Argument('icon', location='json', required=False),)
    @parse_params(Argument('number', location='json', required=False),)
    @swag_from('../swagger/banks/POST.yml')
    def post(**kwargs):
        """ Create a bank based on the sent information """
        logger.debug('Recv %s:%s from %s', req.url, req.data, req.remote_addr)
        data = BankSchema().load(kwargs).data
        bank = BankRepository.create(**data)
        if bank:
            result = BankSchema().dump(bank)
            return {'message': 'Created bank', 'bank': result}, 201
        return {'message': 'Bad request'}, 400  # pragma: no cover
