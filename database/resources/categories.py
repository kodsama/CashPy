#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
import logging
from flasgger import swag_from
from flask_restful import Resource
from flask_restful.reqparse import Argument
from flask import request as req

from .common import parse_params
from schemas import CategorySchema
from repositories import CategoryRepository


logger = logging.getLogger(__name__)


class CategoryResource(Resource):
    """ Verbs relative to the categories """

    @staticmethod
    @swag_from('../swagger/categories/GET.yml')
    def get(id):
        """ Return a category key information based on its id """
        logger.debug('Recv %s:%s from %s', req.url, req.data, req.remote_addr)
        try:
            category = CategoryRepository.get(id=id)
        except Exception as e:
            return {'error': '{}'.format(e)}, 409
        if category:
            result = CategorySchema().dump(category)
            return {'category': result}, 200
        return {'message': 'Category ID {} not found'.format(id)}, 404

    @staticmethod
    @parse_params(Argument('name', location='json', required=False),)
    @parse_params(Argument('icon', location='json', required=False),)
    @parse_params(Argument('parent_id', location='json', required=False),)
    @swag_from('../swagger/categories/PUT.yml')
    def put(**kwargs):
        """ Update a category based on the sent information """
        logger.debug('Recv %s:%s from %s', req.url, req.data, req.remote_addr)
        repository = CategoryRepository()
        data = CategorySchema().load(kwargs).data
        try:
            category = repository.update(**data)
        except Exception as e:
            return {'error': '{}'.format(e)}, 409
        if category:
            result = CategorySchema().dump(category)
            return {'message': 'Updated category', 'category': result}, 200
        return {'message': 'Category ID {} not found'.format(data['id'])}, 404

    @staticmethod
    @swag_from('../swagger/categories/DEL.yml')
    def delete(id):
        ''' Delete a category with provided ID '''
        logger.debug('Recv %s:%s from %s', req.url, req.data, req.remote_addr)
        repository = CategoryRepository()
        try:
            category = repository.delete(id=id)
        except Exception as e:
            return {'error': '{}'.format(e)}, 409
        if category:
            result = CategorySchema().dump(category)
            return {'message': 'Deleted category', 'category': result}, 202
        return {'message': 'Category ID {} not found'.format(id)}, 404


class CategoryListResource(Resource):
    """ Verbs relative to the categories """

    @staticmethod
    @swag_from('../swagger/categories/GET_all.yml')
    def get():
        """ Return all category key information """
        logger.debug('Recv %s:%s from %s', req.url, req.data, req.remote_addr)
        try:
            category = CategoryRepository.get_all()
        except Exception as e:
            return {'error': '{}'.format(e)}, 409
        if category:
            result = CategorySchema(many=True).dump(category)
            return {'category': result}, 200
        return {'message': 'No categories found'}, 404

    @staticmethod
    @parse_params(Argument('name', location='json', required=True),)
    @parse_params(Argument('icon', location='json', required=False),)
    @parse_params(Argument('parent_id', location='json', required=False),)
    @swag_from('../swagger/categories/POST.yml')
    def post(**kwargs):
        """ Create a category based on the sent information """
        logger.debug('Recv %s:%s from %s', req.url, req.data, req.remote_addr)
        data = CategorySchema().load(kwargs).data
        try:
            category = CategoryRepository.create(**data)
        except Exception as e:
            return {'error': '{}'.format(e)}, 409
        if category:
            result = CategorySchema().dump(category)
            return {'message': 'Created category', 'category': result}, 201
        return {'message': 'Bad request'}, 400  # pragma: no cover
