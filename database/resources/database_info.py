#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
import logging
from flasgger import swag_from
from flask_restful import Resource
from flask_restful.reqparse import Argument
from flask import request as req

from .common import parse_params
from schemas import InfoSchema
from repositories import InfoRepository


logger = logging.getLogger(__name__)


class InfoResource(Resource):
    """ Verbs relative to the info """

    @staticmethod
    @swag_from('../swagger/database_info/GET.yml')
    def get():
        """ Return all info key information """
        logger.debug('Recv %s:%s from %s', req.url, req.data, req.remote_addr)
        try:
            info = InfoRepository.get()
        except Exception as e:
            return {'error': '{}'.format(e)}, 409
        if info:
            result = InfoSchema().dump(info)
            return {'info': result}, 200
        return {'message': 'No info found'}, 404

    @staticmethod
    @parse_params(Argument('decimals', location='json', required=False),)
    @parse_params(Argument('default_unit', location='json', required=False),)
    @parse_params(Argument('version', location='json', required=False),)
    @swag_from('../swagger/database_info/PUT.yml')
    def put(**kwargs):
        """ Update an info based on the sent information """
        logger.debug('Recv %s:%s from %s', req.url, req.data, req.remote_addr)
        repository = InfoRepository()
        data = InfoSchema().load(kwargs).data
        try:
            info = repository.update(**data)
        except Exception as e:
            return {'error': '{}'.format(e)}, 409
        if info:
            result = InfoSchema().dump(info)
            return {'message': 'Updated info', 'info': result}, 200
        return {'message': 'No Info found'}, 404

    @staticmethod
    @swag_from('../swagger/database_info/DEL.yml')
    def delete():
        ''' Delete the info entry '''
        logger.debug('Recv %s:%s from %s', req.url, req.data, req.remote_addr)
        repository = InfoRepository()
        try:
            info = repository.delete()
        except Exception as e:
            return {'error': '{}'.format(e)}, 409
        if info:
            result = InfoSchema().dump(info)
            return {'message': 'Deleted info', 'info': result}, 202
        return {'message': 'No info found'}, 404

    @staticmethod
    @parse_params(Argument('decimals', location='json', required=False),)
    @parse_params(Argument('default_unit', location='json', required=False),)
    @parse_params(Argument('version', location='json', required=False),)
    @swag_from('../swagger/database_info/POST.yml')
    def post(**kwargs):
        """ Create an info based on the sent information """
        logger.debug('Recv %s:%s from %s', req.url, req.data, req.remote_addr)
        data = InfoSchema().load(kwargs).data
        try:
            info = InfoRepository.create(**data)
        except Exception as e:
            return {'error': '{}'.format(e)}, 409
        if info:
            result = InfoSchema().dump(info)
            return {'message': 'Created info', 'info': result}, 201
        return {'message': 'Bad request'}, 400  # pragma: no cover
