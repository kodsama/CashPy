#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
import logging
from flasgger import swag_from
from flask_restful import Resource
from flask_restful.reqparse import Argument
from flask import request as req

from .common import parse_params
from schemas import PayeeSchema
from repositories import PayeeRepository


logger = logging.getLogger(__name__)


class PayeeResource(Resource):
    """ Verbs relative to the payees """

    @staticmethod
    @swag_from('../swagger/payees/GET.yml')
    def get(id):
        """ Return an payee key information based on its id """
        logger.debug('Recv %s:%s from %s', req.url, req.data, req.remote_addr)
        try:
            payee = PayeeRepository.get(id=id)
        except Exception as e:
            return {'error': '{}'.format(e)}, 409
        if payee:
            result = PayeeSchema().dump(payee)
            return {'payee': result}, 200
        return {'message': 'Payee ID {} not found'.format(id)}, 404

    @staticmethod
    @parse_params(Argument('address', location='json', required=False),)
    @parse_params(Argument('comment', location='json', required=False),)
    @parse_params(Argument('email', location='json', required=False),)
    @parse_params(Argument('name', location='json', required=False),)
    @parse_params(Argument('phone', location='json', required=False),)
    @parse_params(Argument('website', location='json', required=False),)
    @swag_from('../swagger/payees/PUT.yml')
    def put(**kwargs):
        """ Update an payee based on the sent information """
        logger.debug('Recv %s:%s from %s', req.url, req.data, req.remote_addr)
        repository = PayeeRepository()
        data = PayeeSchema().load(kwargs).data
        try:
            payee = repository.update(**data)
        except Exception as e:
            return {'error': '{}'.format(e)}, 409
        if payee:
            result = PayeeSchema().dump(payee)
            return {'message': 'Updated payee', 'payee': result}, 200
        return {'message': 'Payee ID {} not found'.format(data['id'])}, 404

    @staticmethod
    @swag_from('../swagger/payees/DEL.yml')
    def delete(id):
        ''' Delete a payee with provided ID '''
        logger.debug('Recv %s:%s from %s', req.url, req.data, req.remote_addr)
        repository = PayeeRepository()
        try:
            payee = repository.delete(id=id)
        except Exception as e:
            return {'error': '{}'.format(e)}, 409
        if payee:
            result = PayeeSchema().dump(payee)
            return {'message': 'Deleted payee', 'payee': result}, 202
        return {'message': 'Payee ID {} not found'.format(id)}, 404


class PayeeListResource(Resource):
    """ Verbs relative to the payees """

    @staticmethod
    @swag_from('../swagger/payees/GET_all.yml')
    def get():
        """ Return all payee key information """
        logger.debug('Recv %s:%s from %s', req.url, req.data, req.remote_addr)
        try:
            payee = PayeeRepository.get_all()
        except Exception as e:
            return {'error': '{}'.format(e)}, 409
        if payee:
            result = PayeeSchema(many=True).dump(payee)
            return {'payee': result}, 200
        return {'message': 'No payees found'}, 404

    @staticmethod
    @parse_params(Argument('address', location='json', required=False),)
    @parse_params(Argument('comment', location='json', required=False),)
    @parse_params(Argument('email', location='json', required=False),)
    @parse_params(Argument('name', location='json', required=True),)
    @parse_params(Argument('phone', location='json', required=False),)
    @parse_params(Argument('website', location='json', required=False),)
    @swag_from('../swagger/payees/POST.yml')
    def post(**kwargs):
        """ Create an payee based on the sent information """
        logger.debug('Recv %s:%s from %s', req.url, req.data, req.remote_addr)
        data = PayeeSchema().load(kwargs).data
        try:
            payee = PayeeRepository.create(**data)
        except Exception as e:
            return {'error': '{}'.format(e)}, 409
        if payee:
            result = PayeeSchema().dump(payee)
            return {'message': 'Created payee', 'payee': result}, 201
        return {'message': 'Bad request'}, 400  # pragma: no cover
