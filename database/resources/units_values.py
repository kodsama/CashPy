#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
import logging
from flasgger import swag_from
from flask_restful import Resource
from flask_restful.reqparse import Argument
from flask import request as req

from .common import parse_params
from schemas import UnitValueSchema
from repositories import UnitValueRepository


logger = logging.getLogger(__name__)


class UnitValueResource(Resource):
    """ Verbs relative to the unit_values """

    @staticmethod
    @swag_from('../swagger/units_values/GET.yml')
    def get(id):
        """ Return an unit_value key information based on its id """
        logger.debug('Recv %s:%s from %s', req.url, req.data, req.remote_addr)
        try:
            unit_value = UnitValueRepository.get(id=id)
        except Exception as e:
            return {'error': '{}'.format(e)}, 409
        if unit_value:
            result = UnitValueSchema().dump(unit_value)
            return {'unit_value': result}, 200
        return {'message': 'UnitValue ID {} not found'.format(id)}, 404

    @staticmethod
    @parse_params(Argument('date', location='json', required=False),)
    @parse_params(Argument('unit_id', location='json', required=False),)
    @parse_params(Argument('value', location='json', required=False),)
    @swag_from('../swagger/units_values/PUT.yml')
    def put(**kwargs):
        """ Update an unit_value based on the sent information """
        logger.debug('Recv %s:%s from %s', req.url, req.data, req.remote_addr)
        repository = UnitValueRepository()
        data = UnitValueSchema().load(kwargs).data
        try:
            unit_value = repository.update(**data)
        except Exception as e:
            return {'error': '{}'.format(e)}, 409
        if unit_value:
            result = UnitValueSchema().dump(unit_value)
            return {'message': 'Updated unit_value', 'unit_value': result}, 200
        return {'message': 'UnitValue ID {} not found'.format(data['id'])}, 404

    @staticmethod
    @swag_from('../swagger/units_values/DEL.yml')
    def delete(id):
        ''' Delete a unit_value with provided ID '''
        logger.debug('Recv %s:%s from %s', req.url, req.data, req.remote_addr)
        repository = UnitValueRepository()
        try:
            unit_value = repository.delete(id=id)
        except Exception as e:
            return {'error': '{}'.format(e)}, 409
        if unit_value:
            result = UnitValueSchema().dump(unit_value)
            return {'message': 'Deleted unit_value', 'unit_value': result}, 202
        return {'message': 'UnitValue ID {} not found'.format(id)}, 404


class UnitValueListResource(Resource):
    """ Verbs relative to the unit_values """

    @staticmethod
    @swag_from('../swagger/units_values/GET_all.yml')
    def get():
        """ Return all unit_value key information """
        logger.debug('Recv %s:%s from %s', req.url, req.data, req.remote_addr)
        try:
            unit_value = UnitValueRepository.get_all()
        except Exception as e:
            return {'error': '{}'.format(e)}, 409
        if unit_value:
            result = UnitValueSchema(many=True).dump(unit_value)
            return {'unit_value': result}, 200
        return {'message': 'No unit_values found'}, 404

    @staticmethod
    @parse_params(Argument('date', location='json', required=True),)
    @parse_params(Argument('unit_id', location='json', required=True),)
    @parse_params(Argument('value', location='json', required=True),)
    @swag_from('../swagger/units_values/POST.yml')
    def post(**kwargs):
        """ Create an unit_value based on the sent information """
        logger.debug('Recv %s:%s from %s', req.url, req.data, req.remote_addr)
        data = UnitValueSchema().load(kwargs).data
        try:
            unit_value = UnitValueRepository.create(**data)
        except Exception as e:
            return {'error': '{}'.format(e)}, 409
        if unit_value:
            result = UnitValueSchema().dump(unit_value)
            return {'message': 'Created unit_value', 'unit_value': result}, 201
        return {'message': 'Bad request'}, 400  # pragma: no cover
