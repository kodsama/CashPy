#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
import logging
from flasgger import swag_from
from flask_restful import Resource
from flask_restful.reqparse import Argument
from flask import request as req

from .common import parse_params
from schemas import AccountSchema
from repositories import AccountRepository


logger = logging.getLogger(__name__)


class AccountResource(Resource):
    """ Verbs relative to the accounts """

    @staticmethod
    @swag_from('../swagger/accounts/GET.yml')
    def get(id):
        """ Return an account key information based on its id """
        logger.debug('Recv %s:%s from %s', req.url, req.data, req.remote_addr)
        try:
            account = AccountRepository.get(id=id)
        except Exception as e:
            return {'error': '{}'.format(e)}, 409
        if account:
            result = AccountSchema().dump(account)
            return {'account': result}, 200
        return {'message': 'Account ID {} not found'.format(id)}, 404

    @staticmethod
    @parse_params(Argument('name', location='json', required=False),)
    @parse_params(Argument('bank_id', location='json', required=False),)
    @parse_params(Argument('contact', location='json', required=False),)
    @parse_params(Argument('comment', location='json', required=False),)
    @parse_params(Argument('highlighted', location='json', required=False),)
    @parse_params(Argument('number', location='json', required=False),)
    @parse_params(Argument('opened', location='json', required=False),)
    @parse_params(Argument('typ', location='json', required=False),)
    @swag_from('../swagger/accounts/PUT.yml')
    def put(**kwargs):
        """ Update an account based on the sent information """
        logger.debug('Recv %s:%s from %s', req.url, req.data, req.remote_addr)
        repository = AccountRepository()
        data = AccountSchema().load(kwargs).data
        try:
            account = repository.update(**data)
        except Exception as e:
            return {'error': '{}'.format(e)}, 409
        if account:
            result = AccountSchema().dump(account)
            return {'message': 'Updated account', 'account': result}, 200
        return {'message': 'Account ID {} not found'.format(data['id'])}, 404

    @staticmethod
    @swag_from('../swagger/accounts/DEL.yml')
    def delete(id):
        ''' Delete a account with provided ID '''
        logger.debug('Recv %s:%s from %s', req.url, req.data, req.remote_addr)
        repository = AccountRepository()
        try:
            account = repository.delete(id=id)
        except Exception as e:
            return {'error': '{}'.format(e)}, 409
        if account:
            result = AccountSchema().dump(account)
            return {'message': 'Deleted account', 'account': result}, 202
        return {'message': 'Account ID {} not found'.format(id)}, 404


class AccountListResource(Resource):
    """ Verbs relative to the accounts """

    @staticmethod
    @swag_from('../swagger/accounts/GET_all.yml')
    def get():
        """ Return all account key information """
        logger.debug('Recv %s:%s from %s', req.url, req.data, req.remote_addr)
        try:
            account = AccountRepository.get_all()
        except Exception as e:
            return {'error': '{}'.format(e)}, 409
        if account:
            result = AccountSchema(many=True).dump(account)
            return {'account': result}, 200
        return {'message': 'No accounts found'}, 404

    @staticmethod
    @parse_params(Argument('name', location='json', required=True),)
    @parse_params(Argument('bank_id', location='json', required=False),)
    @parse_params(Argument('contact', location='json', required=False),)
    @parse_params(Argument('comment', location='json', required=False),)
    @parse_params(Argument('highlighted', location='json', required=False),)
    @parse_params(Argument('number', location='json', required=False),)
    @parse_params(Argument('opened', location='json', required=False),)
    @parse_params(Argument('typ', location='json', required=False),)
    @swag_from('../swagger/accounts/POST.yml')
    def post(**kwargs):
        """ Create an account based on the sent information """
        logger.debug('Recv %s:%s from %s', req.url, req.data, req.remote_addr)
        data = AccountSchema().load(kwargs).data
        try:
            account = AccountRepository.create(**data)
        except Exception as e:
            return {'error': '{}'.format(e)}, 409
        if account:
            result = AccountSchema().dump(account)
            return {'message': 'Created account', 'account': result}, 201
        return {'message': 'Bad request'}, 400  # pragma: no cover
