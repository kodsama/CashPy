#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
from flask import Blueprint
from flask_restful import Api

from resources import OperationResource, OperationListResource


OPERATIONS_BLUEPRINT = Blueprint('operations', __name__)

Api(OPERATIONS_BLUEPRINT).add_resource(
    OperationResource,
    '/operations/<int:id>',
    endpoint='operation'
)

Api(OPERATIONS_BLUEPRINT).add_resource(
    OperationListResource,
    '/operations',
    endpoint='operations'
)
