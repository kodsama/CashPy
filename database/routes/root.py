#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
from flask import Blueprint
from flask_restful import Api

from resources import RootResource


ROOT_BLUEPRINT = Blueprint('root', __name__)
Api(ROOT_BLUEPRINT).add_resource(
    RootResource,
    '/'
)
