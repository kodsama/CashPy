#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
from flask import Blueprint
from flask_restful import Api

from resources import AccountResource, AccountListResource


ACCOUNTS_BLUEPRINT = Blueprint('accounts', __name__)

Api(ACCOUNTS_BLUEPRINT).add_resource(
    AccountResource,
    '/accounts/<int:id>',
    endpoint='account'
)

Api(ACCOUNTS_BLUEPRINT).add_resource(
    AccountListResource,
    '/accounts',
    endpoint='accounts'
)
