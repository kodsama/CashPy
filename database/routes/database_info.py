#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
from flask import Blueprint
from flask_restful import Api

from resources import InfoResource


INFO_BLUEPRINT = Blueprint('info', __name__)

Api(INFO_BLUEPRINT).add_resource(
    InfoResource,
    '/info',
    endpoint='info'
)
