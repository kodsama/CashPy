#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
from flask import Blueprint
from flask_restful import Api

from resources import BankResource, BankListResource


BANKS_BLUEPRINT = Blueprint('banks', __name__)

Api(BANKS_BLUEPRINT).add_resource(
    BankResource,
    '/banks/<int:id>',
    endpoint='bank'
)

Api(BANKS_BLUEPRINT).add_resource(
    BankListResource,
    '/banks',
    endpoint='banks'
)
