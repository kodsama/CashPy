#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
from flask import Blueprint
from flask_restful import Api

from resources import CategoryResource, CategoryListResource


CATEGORIES_BLUEPRINT = Blueprint('categories', __name__)

Api(CATEGORIES_BLUEPRINT).add_resource(
    CategoryResource,
    '/categories/<int:id>',
    endpoint='category'
)

Api(CATEGORIES_BLUEPRINT).add_resource(
    CategoryListResource,
    '/categories',
    endpoint='categories'
)
