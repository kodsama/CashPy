#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
from flask import Blueprint
from flask_restful import Api

from resources import UnitOwnedResource, UnitOwnedListResource


UNITS_OWNED_BLUEPRINT = Blueprint('units_owned', __name__)

Api(UNITS_OWNED_BLUEPRINT).add_resource(
    UnitOwnedResource,
    '/units_owned/<int:id>',
    endpoint='unit_owned'
)

Api(UNITS_OWNED_BLUEPRINT).add_resource(
    UnitOwnedListResource,
    '/units_owned',
    endpoint='units_owned'
)
