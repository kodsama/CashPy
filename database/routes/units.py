#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
from flask import Blueprint
from flask_restful import Api

from resources import UnitResource, UnitListResource


UNITS_BLUEPRINT = Blueprint('units', __name__)

Api(UNITS_BLUEPRINT).add_resource(
    UnitResource,
    '/units/<int:id>',
    endpoint='unit'
)

Api(UNITS_BLUEPRINT).add_resource(
    UnitListResource,
    '/units',
    endpoint='units'
)
