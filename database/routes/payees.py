#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
from flask import Blueprint
from flask_restful import Api

from resources import PayeeResource, PayeeListResource


PAYEES_BLUEPRINT = Blueprint('payees', __name__)

Api(PAYEES_BLUEPRINT).add_resource(
    PayeeResource,
    '/payees/<int:id>',
    endpoint='payee'
)

Api(PAYEES_BLUEPRINT).add_resource(
    PayeeListResource,
    '/payees',
    endpoint='payees'
)
