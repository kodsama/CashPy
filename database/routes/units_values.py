#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
from flask import Blueprint
from flask_restful import Api

from resources import UnitValueResource, UnitValueListResource


UNIT_VALUES_BLUEPRINT = Blueprint('units_values', __name__)

Api(UNIT_VALUES_BLUEPRINT).add_resource(
    UnitValueResource,
    '/units_values/<int:id>',
    endpoint='units_value'
)

Api(UNIT_VALUES_BLUEPRINT).add_resource(
    UnitValueListResource,
    '/units_values',
    endpoint='units_values'
)
