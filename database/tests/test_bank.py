#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
import unittest
import json

from models.base import db
from models import BankModel
from repositories import BankRepository
from server import DatabaseServer


db_server = DatabaseServer().server


class TestBank(unittest.TestCase):

    data1 = {'name': 'Test', 'icon': 'Nope', 'number': 'A01', 'id': 1}
    data2 = {'name': 'Test 2', 'icon': 'None', 'number': 'A02', 'id': 2}
    data1_no_id = {'name': 'Test', 'icon': 'Nope', 'number': 'A01'}
    data2_no_id = {'name': 'Test 2', 'icon': 'None', 'number': 'A02'}

    @classmethod
    def setUpClass(cls):
        cls.client = db_server.test_client()

    def setUp(self):
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_get(self):
        """ The GET on /banks with ID should return a bank """
        BankRepository.create(name=self.data1['name'], icon=self.data1['icon'],
                              number=self.data1['number'])
        response = self.client.get('/banks/{}'.format(self.data1['id']))

        self.assertEqual(response.status_code, 200,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json, {'bank': [self.data1, {}]})

    def test_get_fail(self):
        """ The GET on /banks with invalid should not return a bank """
        response = self.client.get('/banks/1')
        self.assertEqual(response.status_code, 404,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'message': 'Bank ID 1 not found'})

    def test_get_all(self):
        """ The GET on /banks without ID should return all banks """
        BankRepository.create(name=self.data1['name'],
                              icon=self.data1['icon'],
                              number=self.data1['number'])
        BankRepository.create(name=self.data2['name'],
                              icon=self.data2['icon'],
                              number=self.data2['number'])
        response = self.client.get('/banks')

        self.assertEqual(response.status_code, 200,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'bank': [[self.data1, self.data2], {}]})

    def test_get_all_empty(self):
        """ The GET on /banks with empty bank data should return 404 """
        response = self.client.get('/banks')
        self.assertEqual(response.status_code, 404,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'message': 'No banks found'})

    def test_create(self):
        """ The POST on /banks should create a bank """
        post_data = {i: self.data1[i] for i in self.data1 if i != 'id'}
        response = self.client.post(
            '/banks',
            content_type='application/json',
            data=json.dumps(post_data)
        )

        # Verify API return
        self.assertEqual(response.status_code, 201,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'message': 'Created bank',
                          'bank': [self.data1, {}]})
        self.assertEqual(BankModel.query.count(), 1)

        # Verify database info
        bank = BankRepository.get(id=self.data1['id'])
        self.assertEqual(bank.name, self.data1['name'])
        self.assertEqual(bank.icon, self.data1['icon'])
        self.assertEqual(bank.number, self.data1['number'])

    def test_create_fail(self):
        """ The bad POST on /banks should NOT create a bank """
        post_data = {'plop': 'Test'}
        response = self.client.post(
            '/banks',
            content_type='application/json',
            data=json.dumps(post_data)
        )

        # Verify API return
        self.assertEqual(response.status_code, 400)

    def test_update(self):
        """ The PUT on /banks should update an bank's info """
        BankRepository.create(name=self.data1['name'],
                              icon=self.data1['icon'],
                              number=self.data1['number'])
        put_data = {k: v for k, v in self.data2_no_id.items()}
        put_data['id'] = self.data1['id']
        response = self.client.put(
            '/banks/{}'.format(put_data['id']),
            content_type='application/json',
            data=json.dumps(put_data)
        )

        # Verify API return
        self.assertEqual(response.status_code, 200,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'message': 'Updated bank',
                          'bank': [put_data, {}]})

        # Verify database info
        bank = BankRepository.get(id=put_data['id'])
        self.assertEqual(bank.name, self.data2['name'])
        self.assertEqual(bank.icon, self.data2['icon'])
        self.assertEqual(bank.number, self.data2['number'])

    def test_update_fail(self):
        """ The PUT on /banks with bad ID should not update a bank's info """
        BankRepository.create(name=self.data1['name'], icon=self.data1['icon'],
                              number=self.data1['number'])
        response = self.client.put(
            '/banks/2',
            content_type='application/json',
            data=json.dumps(self.data2_no_id)
        )

        # Verify API return
        self.assertEqual(response.status_code, 404,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'message': 'Bank ID 2 not found'})

        # Verify database info
        bank = BankRepository.get(id=self.data1['id'])
        self.assertEqual(bank.name, self.data1['name'])
        self.assertEqual(bank.icon, self.data1['icon'])
        self.assertEqual(bank.number, self.data1['number'])

    def test_delete(self):
        """ The DEL on /banks should delete a bank """
        BankRepository.create(name=self.data1['name'], icon=self.data1['icon'],
                              number=self.data1['number'])
        BankRepository.create(name=self.data2['name'], icon=self.data2['icon'],
                              number=self.data2['number'])
        bank = BankRepository.get(id=self.data1['id'])
        self.assertEqual(bank.id, self.data1['id'])

        response = self.client.delete('/banks/{}'.format(self.data1['id']))

        # Verify API return
        self.assertEqual(response.status_code, 202)
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'message': 'Deleted bank', 'bank': [self.data1, {}]})

        # Verify database info
        bank = BankRepository.get(id=self.data1['id'])
        self.assertEqual(bank, None)

    def test_delete_fail(self):
        """ The DEL on /banks should delete a bank """
        BankRepository.create(name=self.data1['name'], icon=self.data1['icon'],
                              number=self.data1['number'])
        BankRepository.create(name=self.data2['name'], icon=self.data2['icon'],
                              number=self.data2['number'])
        bank = BankRepository.get(id=self.data1['id'])
        self.assertEqual(bank.id, self.data1['id'])

        response = self.client.delete('/banks/3')

        # Verify API return
        self.assertEqual(response.status_code, 404,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'message': 'Bank ID 3 not found'})

        # Verify database info
        bank = BankRepository.get(id=self.data1['id'])
        self.assertNotEqual(bank, None)
        bank = BankRepository.get(id=self.data2['id'])
        self.assertNotEqual(bank, None)

    def test_repr(self):
        """ Verify the repr function of the model works """
        self.assertIsInstance(str(BankModel(**self.data1_no_id)), str)
