#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
import unittest
import json

from models.base import db
from models import UnitValueModel
from repositories import UnitRepository, UnitValueRepository
from server import DatabaseServer


db_server = DatabaseServer().server


class TestUnitValue(unittest.TestCase):

    unit_data_no_id = {'name': 'Test 1', 'symbol': 'X'}
    data1 = {'id': 1, 'date': '2001-01-01T01:01:01+00:00',
             'unit_id': 1, 'value': 1}
    data2 = {'id': 2, 'date': '2002-02-02T02:02:02+00:00',
             'unit_id': 1, 'value': 2}
    data1_no_id = {'date': '2001-01-01T01:01:01+00:00',
                   'unit_id': 1, 'value': 1}
    data2_no_id = {'date': '2002-02-02T02:02:02+00:00',
                   'unit_id': 1, 'value': 2}

    @classmethod
    def setUpClass(cls):
        cls.client = db_server.test_client()

    def setUp(self):
        db.create_all()
        UnitRepository.create(**self.unit_data_no_id)

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_get(self):
        """ GET on /units_values with ID should return a units_value """
        UnitValueRepository.create(**self.data1_no_id)
        response = self.client.get('/units_values/{}'.format(self.data1['id']))

        self.assertEqual(response.status_code, 200,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json, {'unit_value': [self.data1, {}]})

    def test_get_fail(self):
        """ GET on /units_values with invalid should not return  """
        # Get unknown units_value
        response = self.client.get('/units_values/1')
        self.assertEqual(response.status_code, 404,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'message': 'UnitValue ID 1 not found'})

    def test_get_all(self):
        """ GET on /units_values without ID should return all  """
        UnitValueRepository.create(**self.data1_no_id)
        UnitValueRepository.create(**self.data2_no_id)
        response = self.client.get('/units_values')

        self.assertEqual(response.status_code, 200,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'unit_value': [[self.data1, self.data2], {}]})

    def test_get_all_empty(self):
        """ GET on /units_values with empty data should return 404 """
        response = self.client.get('/units_values')
        self.assertEqual(response.status_code, 404,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'message': 'No unit_values found'})

    def test_create(self):
        """ POST on /units_values should create a units_value """
        response = self.client.post(
            '/units_values',
            content_type='application/json',
            data=json.dumps(self.data1_no_id)
        )

        # Verify API return
        self.assertEqual(response.status_code, 201,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'message': 'Created unit_value',
                          'unit_value': [self.data1, {}]})
        self.assertEqual(UnitValueModel.query.count(), 1)

        # Verify database info
        units_value = UnitValueRepository.get(id=self.data1['id'])
        self.assertEqual(units_value.value, self.data1['value'])
        self.assertEqual(units_value.id, self.data1['id'])
        self.assertEqual(units_value.date.strftime('%Y-%m-%dT%H:%M:%S+00:00'),
                         self.data1['date'])
        self.assertEqual(units_value.unit_id, self.data1['unit_id'])

    def test_create_fail(self):
        """ bad POST on /units_values should NOT create a units_value """
        post_data = {'plop': 'Test'}
        response = self.client.post(
            '/units_values',
            content_type='application/json',
            data=json.dumps(post_data)
        )

        # Verify API return
        self.assertEqual(response.status_code, 400,
                         msg=json.loads(response.data.decode('utf-8')))

    def test_update(self):
        """ PUT on /units_values should update an units_value's info """
        UnitValueRepository.create(**self.data1_no_id)
        put_data = {k: v for k, v in self.data2_no_id.items()}
        put_data['id'] = self.data1['id']
        response = self.client.put(
            '/units_values/{}'.format(put_data['id']),
            content_type='application/json',
            data=json.dumps(put_data)
        )

        # Verify API return
        self.assertEqual(response.status_code, 200,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'message': 'Updated unit_value',
                          'unit_value': [put_data, {}]})

        # Verify database info
        units_value = UnitValueRepository.get(id=put_data['id'])
        self.assertEqual(units_value.unit_id, put_data['unit_id'])
        self.assertEqual(units_value.id, put_data['id'])
        self.assertEqual(units_value.date.strftime('%Y-%m-%dT%H:%M:%S+00:00'),
                         put_data['date'])
        self.assertEqual(units_value.value, put_data['value'])

    def test_update_fail(self):
        """ PUT on /units_values with bad info should not update """
        # Bad unit_value ID
        UnitValueRepository.create(**self.data1_no_id)
        response = self.client.put(
            '/units_values/{}'.format(self.data2['id']),
            content_type='application/json',
            data=json.dumps(self.data2_no_id)
        )

        self.assertEqual(response.status_code, 404,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'message': 'UnitValue ID 2 not found'})

        # Verify database info
        units_value = UnitValueRepository.get(id=self.data1['id'])
        self.assertEqual(units_value.value, self.data1['value'])
        self.assertEqual(units_value.id, self.data1['id'])
        self.assertEqual(units_value.date.strftime('%Y-%m-%dT%H:%M:%S+00:00'),
                         self.data1['date'])
        self.assertEqual(units_value.unit_id, self.data1['unit_id'])

    def test_delete(self):
        """ DEL on /units_values should delete a units_value """
        UnitValueRepository.create(**self.data1_no_id)
        UnitValueRepository.create(**self.data2_no_id)
        units_value = UnitValueRepository.get(id=self.data1['id'])
        self.assertEqual(units_value.id, self.data1['id'])

        response = self.client.delete('/units_values/{}'.format(
                                      self.data1['id']))

        # Verify API return
        self.assertEqual(response.status_code, 202)
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'message': 'Deleted unit_value',
                          'unit_value': [self.data1, {}]})

        # Verify database info
        units_value = UnitValueRepository.get(id=self.data1['id'])
        self.assertEqual(units_value, None)

    def test_delete_fail(self):
        """ DEL on /units_values should delete a units_value """
        UnitValueRepository.create(**self.data1_no_id)
        UnitValueRepository.create(**self.data2_no_id)
        units_value = UnitValueRepository.get(id=self.data1['id'])
        self.assertEqual(units_value.id, self.data1['id'])

        response = self.client.delete('/units_values/3')

        # Verify API return
        self.assertEqual(response.status_code, 404,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'message': 'UnitValue ID 3 not found'})

        # Verify database info
        units_value = UnitValueRepository.get(id=self.data1['id'])
        self.assertNotEqual(units_value, None)
        units_value = UnitValueRepository.get(id=self.data2['id'])
        self.assertNotEqual(units_value, None)

    def test_repr(self):
        """ Verify the repr function of the model works """
        self.assertIsInstance(str(UnitValueModel(**self.data1)), str)
