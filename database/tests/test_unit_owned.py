#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
import unittest
import json

from models.base import db
from models import UnitOwnedModel
from repositories import UnitRepository, UnitOwnedRepository
from server import DatabaseServer


db_server = DatabaseServer().server


class TestUnitOwned(unittest.TestCase):

    unit_data_no_id = {'name': 'Test 1', 'symbol': 'X'}
    data1 = {'id': 1, 'amount': 1, 'date': '2001-01-01T01:01:01+00:00',
             'parent_unit_id': 1}
    data2 = {'id': 2, 'amount': 2, 'date': '2002-02-02T02:02:02+00:00',
             'parent_unit_id': 1}
    data1_no_id = {'amount': 1, 'date': '2001-01-01T01:01:01+00:00',
                   'parent_unit_id': 1}
    data2_no_id = {'amount': 2, 'date': '2002-02-02T02:02:02+00:00',
                   'parent_unit_id': 1}

    @classmethod
    def setUpClass(cls):
        cls.client = db_server.test_client()

    def setUp(self):
        db.create_all()
        UnitRepository.create(**self.unit_data_no_id)

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_get(self):
        """ GET on /units_owned with ID should return a unit_owned """
        UnitOwnedRepository.create(**self.data1_no_id)
        response = self.client.get('/units_owned/{}'.format(self.data1['id']))

        self.assertEqual(response.status_code, 200,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json, {'unit_owned': [self.data1, {}]})

    def test_get_fail(self):
        """ GET on /units_owned with invalid should not return units_owned """
        # Get unknown unit_owned
        response = self.client.get('/units_owned/1')
        self.assertEqual(response.status_code, 404,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'message': 'UnitOwned ID 1 not found'})

    def test_get_all(self):
        """ GET on /units_owned without ID should return all units_owned """
        UnitOwnedRepository.create(**self.data1_no_id)
        UnitOwnedRepository.create(**self.data2_no_id)
        response = self.client.get('/units_owned')

        self.assertEqual(response.status_code, 200,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'unit_owned': [[self.data1, self.data2], {}]})

    def test_get_all_empty(self):
        """ GET on /units_owned with empty data should return 404 """
        response = self.client.get('/units_owned')
        self.assertEqual(response.status_code, 404,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'message': 'No units_owned found'})

    def test_create(self):
        """ POST on /units_owned should create a unit_owned """
        response = self.client.post(
            '/units_owned',
            content_type='application/json',
            data=json.dumps(self.data1_no_id)
        )

        # Verify API return
        self.assertEqual(response.status_code, 201,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'message': 'Created unit_owned',
                          'unit_owned': [self.data1, {}]})
        self.assertEqual(UnitOwnedModel.query.count(), 1)

        # Verify database info
        unit_owned = UnitOwnedRepository.get(id=self.data1['id'])
        self.assertEqual(unit_owned.amount, self.data1['amount'])
        self.assertEqual(unit_owned.id, self.data1['id'])
        self.assertEqual(unit_owned.date.strftime('%Y-%m-%dT%H:%M:%S+00:00'),
                         self.data1['date'])
        self.assertEqual(unit_owned.parent_unit_id,
                         self.data1['parent_unit_id'])

    def test_create_fail(self):
        """ bad POST on /units_owned should NOT create a unit_owned """
        post_data = {'plop': 'Test'}
        response = self.client.post(
            '/units_owned',
            content_type='application/json',
            data=json.dumps(post_data)
        )

        # Verify API return
        self.assertEqual(response.status_code, 400,
                         msg=json.loads(response.data.decode('utf-8')))

    def test_update(self):
        """ PUT on /units_owned should update an unit_owned's info """
        UnitOwnedRepository.create(**self.data2_no_id)
        put_data = {k: v for k, v in self.data2_no_id.items()}
        put_data['id'] = self.data1['id']
        response = self.client.put(
            '/units_owned/{}'.format(put_data['id']),
            content_type='application/json',
            data=json.dumps(put_data)
        )

        # Verify API return
        self.assertEqual(response.status_code, 200,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'message': 'Updated unit_owned',
                          'unit_owned': [put_data, {}]})

        # Verify database info
        unit_owned = UnitOwnedRepository.get(id=put_data['id'])
        self.assertEqual(unit_owned.amount, put_data['amount'])
        self.assertEqual(unit_owned.id, put_data['id'])
        self.assertEqual(unit_owned.date.strftime('%Y-%m-%dT%H:%M:%S+00:00'),
                         put_data['date'])
        self.assertEqual(unit_owned.parent_unit_id,
                         put_data['parent_unit_id'])

    def test_update_fail(self):
        """ PUT on /units_owned with bad info should not update """
        # Bad unit_owned ID
        UnitOwnedRepository.create(**self.data1_no_id)
        response = self.client.put(
            '/units_owned/{}'.format(self.data2['id']),
            content_type='application/json',
            data=json.dumps(self.data2_no_id)
        )
        self.assertEqual(response.status_code, 404,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'message': 'UnitOwned ID 2 not found'})

        # Verify database info
        unit_owned = UnitOwnedRepository.get(id=self.data1['id'])
        self.assertEqual(unit_owned.amount, self.data1['amount'])
        self.assertEqual(unit_owned.id, self.data1['id'])
        self.assertEqual(unit_owned.date.strftime('%Y-%m-%dT%H:%M:%S+00:00'),
                         self.data1['date'])
        self.assertEqual(unit_owned.parent_unit_id,
                         self.data1['parent_unit_id'])

    def test_delete(self):
        """ DEL on /units_owned should delete a unit_owned """
        UnitOwnedRepository.create(**self.data1_no_id)
        UnitOwnedRepository.create(**self.data2_no_id)
        unit_owned = UnitOwnedRepository.get(id=self.data1['id'])
        self.assertEqual(unit_owned.id, self.data1['id'])

        response = self.client.delete('/units_owned/{}'.format(
                                      self.data1['id']))

        # Verify API return
        self.assertEqual(response.status_code, 202)
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'message': 'Deleted unit_owned',
                          'unit_owned': [self.data1, {}]})

        # Verify database info
        unit_owned = UnitOwnedRepository.get(id=self.data1['id'])
        self.assertEqual(unit_owned, None)

    def test_delete_fail(self):
        """ DEL on /units_owned should delete a unit_owned """
        UnitOwnedRepository.create(**self.data1_no_id)
        UnitOwnedRepository.create(**self.data2_no_id)
        unit_owned = UnitOwnedRepository.get(id=self.data1['id'])
        self.assertEqual(unit_owned.id, self.data1['id'])

        response = self.client.delete('/units_owned/3')

        # Verify API return
        self.assertEqual(response.status_code, 404,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'message': 'UnitOwned ID 3 not found'})

        # Verify database info
        unit_owned = UnitOwnedRepository.get(id=self.data1['id'])
        self.assertNotEqual(unit_owned, None)
        unit_owned = UnitOwnedRepository.get(id=self.data2['id'])
        self.assertNotEqual(unit_owned, None)

    def test_repr(self):
        """ Verify the repr function of the model works """
        self.assertIsInstance(str(UnitOwnedModel(**self.data1_no_id)), str)
