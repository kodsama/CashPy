#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
import unittest
import json

from models.base import db
from models import AccountModel
from repositories import AccountRepository, BankRepository
from server import DatabaseServer


db_server = DatabaseServer().server


class TestAccount(unittest.TestCase):

    bank_data1_no_id = {'name': 'Test', 'icon': 'Nope', 'number': 'A01'}
    data1 = {'id': 1, 'bank_id': 1, 'comment': 'A comment',
             'contact': 'Joe Doe', 'highlighted': False,
             'name': 'Saving pennies', 'number': 'X', 'opened': True,
             'typ': 'Savings'}
    data2 = {'id': 2, 'bank_id': 1, 'comment': 'Another',
             'contact': 'Heisenberg', 'highlighted': True,
             'name': 'Saving cash', 'number': '69', 'opened': False,
             'typ': 'Savings'}
    data1_no_id = {'bank_id': 1, 'comment': 'A comment',
                   'contact': 'Joe Doe', 'highlighted': False,
                   'name': 'Saving pennies', 'number': 'X', 'opened': True,
                   'typ': 'Savings'}
    data2_no_id = {'bank_id': 1, 'comment': 'Another',
                   'contact': 'Heisenberg', 'highlighted': True,
                   'name': 'Saving cash', 'number': '69', 'opened': False,
                   'typ': 'Savings'}

    @classmethod
    def setUpClass(cls):
        cls.client = db_server.test_client()

    def setUp(self):
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_get(self):
        """ The GET on /accounts with ID should return a account """
        BankRepository.create(**self.bank_data1_no_id)
        AccountRepository.create(**self.data1_no_id)
        response = self.client.get('/accounts/{}'.format(self.data1['id']))

        self.assertEqual(response.status_code, 200,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json, {'account': [self.data1, {}]})

    def test_get_fail(self):
        """ The GET on /accounts with invalid should not return a account """
        # Get unknown account
        response = self.client.get('/accounts/1')
        self.assertEqual(response.status_code, 404,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'message': 'Account ID 1 not found'})

    def test_get_all(self):
        """ The GET on /accounts without ID should return all accounts """
        BankRepository.create(**self.bank_data1_no_id)
        AccountRepository.create(**self.data1_no_id)
        AccountRepository.create(**self.data2_no_id)
        response = self.client.get('/accounts')

        self.assertEqual(response.status_code, 200,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'account': [[self.data1, self.data2], {}]})

    def test_get_all_empty(self):
        """ The GET on /accounts with empty account data should return 404 """
        response = self.client.get('/accounts')
        self.assertEqual(response.status_code, 404,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'message': 'No accounts found'})

    def test_create(self):
        """ The POST on /accounts should create a account """
        BankRepository.create(**self.bank_data1_no_id)
        response = self.client.post(
            '/accounts',
            content_type='application/json',
            data=json.dumps(self.data1_no_id)
        )

        # Verify API return
        self.assertEqual(response.status_code, 201,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'message': 'Created account',
                          'account': [self.data1, {}]})
        self.assertEqual(AccountModel.query.count(), 1)

        # Verify database info
        account = AccountRepository.get(id=self.data1['id'])
        self.assertEqual(account.name, self.data1['name'])
        self.assertEqual(account.id, self.data1['id'])
        self.assertEqual(account.bank_id, self.data1['bank_id'])
        self.assertEqual(account.number, self.data1['number'])
        self.assertEqual(account.comment, self.data1['comment'])
        self.assertEqual(account.contact, self.data1['contact'])
        self.assertEqual(account.typ, self.data1['typ'])
        self.assertIsInstance(account.opened, bool)
        self.assertIsInstance(account.highlighted, bool)

    def test_create_fail(self):
        """ The bad POST on /accounts should NOT create a account """
        post_data = {'plop': 'Test'}
        response = self.client.post(
            '/accounts',
            content_type='application/json',
            data=json.dumps(post_data)
        )

        # Verify API return
        self.assertEqual(response.status_code, 400,
                         msg=json.loads(response.data.decode('utf-8')))

    def test_update(self):
        """ The PUT on /accounts should update an account's info """
        BankRepository.create(**self.bank_data1_no_id)
        AccountRepository.create(**self.data2_no_id)
        put_data = {k: v for k, v in self.data2_no_id.items()}
        put_data['id'] = self.data1['id']
        response = self.client.put(
            '/accounts/{}'.format(put_data['id']),
            content_type='application/json',
            data=json.dumps(put_data)
        )

        # Verify API return
        self.assertEqual(response.status_code, 200,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'message': 'Updated account',
                          'account': [put_data, {}]})

        # Verify database info
        account = AccountRepository.get(id=put_data['id'])
        self.assertEqual(account.name, put_data['name'])
        self.assertEqual(account.id, put_data['id'])
        self.assertEqual(account.bank_id, put_data['bank_id'])
        self.assertEqual(account.number, put_data['number'])
        self.assertEqual(account.comment, put_data['comment'])
        self.assertEqual(account.contact, put_data['contact'])
        self.assertEqual(account.typ, put_data['typ'])
        self.assertIsInstance(account.opened, bool)
        self.assertIsInstance(account.highlighted, bool)

    def test_update_fail(self):
        """ The PUT on /accounts with bad info should not update an account """
        # Bad account ID
        BankRepository.create(**self.bank_data1_no_id)
        AccountRepository.create(**self.data1_no_id)
        response = self.client.put(
            '/accounts/{}'.format(self.data2['id']),
            content_type='application/json',
            data=json.dumps(self.data2_no_id)
        )
        self.assertEqual(response.status_code, 404,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'message': 'Account ID 2 not found'})

        # Verify database info
        account = AccountRepository.get(id=self.data1['id'])
        self.assertEqual(account.name, self.data1['name'])
        self.assertEqual(account.id, self.data1['id'])
        self.assertEqual(account.bank_id, self.data1['bank_id'])
        self.assertEqual(account.number, self.data1['number'])
        self.assertEqual(account.comment, self.data1['comment'])
        self.assertEqual(account.contact, self.data1['contact'])
        self.assertEqual(account.typ, self.data1['typ'])
        self.assertIsInstance(account.opened, bool)
        self.assertIsInstance(account.highlighted, bool)

    def test_delete(self):
        """ The DEL on /accounts should delete a account """
        BankRepository.create(**self.bank_data1_no_id)
        AccountRepository.create(**self.data1_no_id)
        AccountRepository.create(**self.data2_no_id)
        account = AccountRepository.get(id=self.data1['id'])
        self.assertEqual(account.id, self.data1['id'])

        response = self.client.delete('/accounts/{}'.format(self.data1['id']))

        # Verify API return
        self.assertEqual(response.status_code, 202)
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'message': 'Deleted account',
                          'account': [self.data1, {}]})

        # Verify database info
        account = AccountRepository.get(id=self.data1['id'])
        self.assertEqual(account, None)

    def test_delete_fail(self):
        """ The DEL on /accounts should delete a account """
        BankRepository.create(**self.bank_data1_no_id)
        AccountRepository.create(**self.data1_no_id)
        AccountRepository.create(**self.data2_no_id)
        account = AccountRepository.get(id=self.data1['id'])
        self.assertEqual(account.id, self.data1['id'])

        response = self.client.delete('/accounts/3')

        # Verify API return
        self.assertEqual(response.status_code, 404,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'message': 'Account ID 3 not found'})

        # Verify database info
        account = AccountRepository.get(id=self.data1['id'])
        self.assertNotEqual(account, None)
        account = AccountRepository.get(id=self.data2['id'])
        self.assertNotEqual(account, None)

    def test_repr(self):
        """ Verify the repr function of the model works """
        self.assertIsInstance(str(AccountModel(**self.data1_no_id)), str)
