#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
import unittest
import json

from models.base import db
from models import PayeeModel
from repositories import PayeeRepository
from server import DatabaseServer


db_server = DatabaseServer().server


class TestPayee(unittest.TestCase):

    data1 = {'id': 1, 'address': 'Nowhere', 'comment': 'A comment',
             'email': 'Nowhere@rainbow', 'name': 'Joe Doe', 'phone': '00 01',
             'website': 'https://nothing.com'}
    data2 = {'id': 2, 'address': 'Somewhere', 'comment': 'A comment 2',
             'email': 'somewhere@rainbow', 'name': 'Jack Rainbow',
             'phone': '00 02', 'website': 'https://something.com'}
    data1_no_id = {'address': 'Nowhere', 'comment': 'A comment',
                   'email': 'Nowhere@rainbow', 'name': 'Joe Doe',
                   'phone': '00 01', 'website': 'https://nothing.com'}
    data2_no_id = {'address': 'Somewhere', 'comment': 'A comment 2',
                   'email': 'somewhere@rainbow', 'name': 'Jack Rainbow',
                   'phone': '00 02', 'website': 'https://something.com'}

    @classmethod
    def setUpClass(cls):
        cls.client = db_server.test_client()

    def setUp(self):
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_get(self):
        """ The GET on /payees with ID should return a payee """
        PayeeRepository.create(**self.data1_no_id)
        response = self.client.get('/payees/{}'.format(self.data1['id']))

        self.assertEqual(response.status_code, 200,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json, {'payee': [self.data1, {}]})

    def test_get_fail(self):
        """ The GET on /payees with invalid should not return a payee """
        # Get unknown payee
        response = self.client.get('/payees/1')
        self.assertEqual(response.status_code, 404,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'message': 'Payee ID 1 not found'})

    def test_get_all(self):
        """ The GET on /payees without ID should return all payees """
        PayeeRepository.create(**self.data1_no_id)
        PayeeRepository.create(**self.data2_no_id)
        response = self.client.get('/payees')

        self.assertEqual(response.status_code, 200,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'payee': [[self.data1, self.data2], {}]})

    def test_get_all_empty(self):
        """ The GET on /payees with empty payee data should return 404 """
        response = self.client.get('/payees')
        self.assertEqual(response.status_code, 404,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'message': 'No payees found'})

    def test_create(self):
        """ The POST on /payees should create a payee """
        response = self.client.post(
            '/payees',
            content_type='application/json',
            data=json.dumps(self.data1_no_id)
        )

        # Verify API return
        self.assertEqual(response.status_code, 201,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'message': 'Created payee',
                          'payee': [self.data1, {}]})
        self.assertEqual(PayeeModel.query.count(), 1)

        # Verify database info
        payee = PayeeRepository.get(id=self.data1['id'])
        self.assertEqual(payee.address, self.data1['address'])
        self.assertEqual(payee.comment, self.data1['comment'])
        self.assertEqual(payee.id, self.data1['id'])
        self.assertEqual(payee.name, self.data1['name'])
        self.assertEqual(payee.phone, self.data1['phone'])
        self.assertEqual(payee.website, self.data1['website'])

    def test_create_fail(self):
        """ The bad POST on /payees should NOT create a payee """
        post_data = {'plop': 'Test'}
        response = self.client.post(
            '/payees',
            content_type='application/json',
            data=json.dumps(post_data)
        )

        # Verify API return
        self.assertEqual(response.status_code, 400,
                         msg=json.loads(response.data.decode('utf-8')))

    def test_update(self):
        """ The PUT on /payees should update an payee's info """
        PayeeRepository.create(**self.data2_no_id)
        put_data = {k: v for k, v in self.data2_no_id.items()}
        put_data['id'] = self.data1['id']
        response = self.client.put(
            '/payees/{}'.format(put_data['id']),
            content_type='application/json',
            data=json.dumps(put_data)
        )

        # Verify API return
        self.assertEqual(response.status_code, 200,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'message': 'Updated payee',
                          'payee': [put_data, {}]})

        # Verify database info
        payee = PayeeRepository.get(id=put_data['id'])
        self.assertEqual(payee.id, self.data1['id'])
        self.assertEqual(payee.address, self.data2['address'])
        self.assertEqual(payee.comment, self.data2['comment'])
        self.assertEqual(payee.name, self.data2['name'])
        self.assertEqual(payee.phone, self.data2['phone'])
        self.assertEqual(payee.website, self.data2['website'])

    def test_update_fail(self):
        """ The PUT on /payees with bad info should not update an payee """
        PayeeRepository.create(**self.data1_no_id)
        # Bad payee ID
        response = self.client.put(
            '/payees/{}'.format(self.data2['id']),
            content_type='application/json',
            data=json.dumps(self.data2_no_id)
        )
        self.assertEqual(response.status_code, 404,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'message': 'Payee ID 2 not found'})

        # Verify database info
        payee = PayeeRepository.get(id=self.data1['id'])
        self.assertEqual(payee.address, self.data1['address'])
        self.assertEqual(payee.comment, self.data1['comment'])
        self.assertEqual(payee.id, self.data1['id'])
        self.assertEqual(payee.name, self.data1['name'])
        self.assertEqual(payee.phone, self.data1['phone'])
        self.assertEqual(payee.website, self.data1['website'])

    def test_delete(self):
        """ The DEL on /payees should delete a payee """
        PayeeRepository.create(**self.data1_no_id)
        PayeeRepository.create(**self.data2_no_id)
        payee = PayeeRepository.get(id=self.data1['id'])
        self.assertEqual(payee.id, self.data1['id'])

        response = self.client.delete('/payees/{}'.format(self.data1['id']))

        # Verify API return
        self.assertEqual(response.status_code, 202)
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'message': 'Deleted payee',
                          'payee': [self.data1, {}]})

        # Verify database info
        payee = PayeeRepository.get(id=self.data1['id'])
        self.assertEqual(payee, None)

    def test_delete_fail(self):
        """ The DEL on /payees should delete a payee """
        PayeeRepository.create(**self.data1_no_id)
        PayeeRepository.create(**self.data2_no_id)
        payee = PayeeRepository.get(id=self.data1['id'])
        self.assertEqual(payee.id, self.data1['id'])

        response = self.client.delete('/payees/3')

        # Verify API return
        self.assertEqual(response.status_code, 404,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'message': 'Payee ID 3 not found'})

        # Verify database info
        payee = PayeeRepository.get(id=self.data1['id'])
        self.assertNotEqual(payee, None)
        payee = PayeeRepository.get(id=self.data2['id'])
        self.assertNotEqual(payee, None)

    def test_repr(self):
        """ Verify the repr function of the model works """
        self.assertIsInstance(str(PayeeModel(**self.data1_no_id)), str)
