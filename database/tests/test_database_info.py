#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
import unittest
import json

from models.base import db
from models import InfoModel
from repositories import InfoRepository
from server import DatabaseServer
from datetime import datetime


db_server = DatabaseServer().server


class TestInfo(unittest.TestCase):

    data1 = {'decimals': 1, 'default_unit': 1, 'version': '1'}
    data2 = {'decimals': 2, 'default_unit': 2, 'version': '2'}

    @classmethod
    def setUpClass(cls):
        cls.client = db_server.test_client()

    def setUp(self):
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_get(self):
        """ The GET on /info return the info if it exists """
        InfoRepository.create(**self.data1)
        response = self.client.get('/info')

        self.assertEqual(response.status_code, 200,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        response_json = response_json['info'][0]    # Get first

        self.assertEqual(response_json['decimals'], self.data1['decimals'])
        self.assertEqual(response_json['default_unit'],
                         self.data1['default_unit'])
        self.assertEqual(response_json['version'], self.data1['version'])
        self.assertIsInstance(response_json['created'], str)
        self.assertIsInstance(response_json['updated'], str)

    def test_get_empty(self):
        """ The GET on /info with invalid should not return a info """
        # Get unknown info
        response = self.client.get('/info')
        self.assertEqual(response.status_code, 404,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'message': 'No info found'})

    def test_create(self):
        """ The POST on /info should create an info """
        response = self.client.post(
            '/info',
            content_type='application/json',
            data=json.dumps(self.data1)
        )

        # Verify API return
        self.assertEqual(response.status_code, 201,
                         msg=json.loads(response.data.decode('utf-8')))
        json.loads(response.data.decode('utf-8'))
        self.assertEqual(InfoModel.query.count(), 1)

        # Verify database info
        info = InfoRepository.get()
        self.assertEqual(info.decimals, self.data1['decimals'])
        self.assertEqual(info.default_unit, self.data1['default_unit'])
        self.assertEqual(info.version, self.data1['version'])
        self.assertIsInstance(info.created, datetime)
        self.assertIsInstance(info.updated, datetime)

    def test_create_fail(self):
        """ The bad POST on /info should NOT create a info """
        post_data = {'plop': 'Test'}
        response = self.client.post(
            '/info',
            content_type='application/json',
            data=json.dumps(post_data)
        )

        # Verify API return
        self.assertEqual(response.status_code, 409,
                         msg=json.loads(response.data.decode('utf-8')))

    def test_update(self):
        """ The PUT on /info should update an info's info """
        InfoRepository.create(**self.data1)
        created = InfoRepository.get().created
        updated = InfoRepository.get().updated
        response = self.client.put(
            '/info',
            content_type='application/json',
            data=json.dumps(self.data2)
        )

        # Verify API return
        self.assertEqual(response.status_code, 200,
                         msg=json.loads(response.data.decode('utf-8')))
        json.loads(response.data.decode('utf-8'))

        # Verify database info
        info = InfoRepository.get()
        self.assertEqual(info.decimals, self.data2['decimals'])
        self.assertEqual(info.default_unit, self.data2['default_unit'])
        self.assertEqual(info.version, self.data2['version'])
        self.assertEqual(info.created, created)
        self.assertNotEqual(info.updated, updated)

    def test_update_fail(self):
        """ The PUT on /info with bad info should not update an info """
        # Try to update a non existing entry
        response = self.client.put(
            '/info',
            content_type='application/json',
            data=json.dumps(self.data2)
        )
        self.assertEqual(response.status_code, 404,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'message': 'No Info found'})

        # Verify database info
        info = InfoRepository.get()
        self.assertIsNone(info)

    def test_delete(self):
        """ The DEL on /info should delete a info """
        InfoRepository.create(**self.data1)
        info = InfoRepository.get()
        self.assertIsNotNone(info)

        response = self.client.delete('/info')

        # Verify API return
        self.assertEqual(response.status_code, 202)
        json.loads(response.data.decode('utf-8'))

        # Verify database info
        info = InfoRepository.get()
        self.assertIsNone(info)

    def test_delete_fail(self):
        """ The DEL on /info with no data should not delete anything """
        response = self.client.delete('/info')

        # Verify API return
        self.assertEqual(response.status_code, 404,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'message': 'No info found'})

        # Verify database info
        info = InfoRepository.get()
        self.assertIsNone(info)

    def test_repr(self):
        """ Verify the repr function of the model works """
        self.assertIsInstance(str(InfoModel(**self.data1)), str)
