#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
import unittest

from models.base import db
from server import DatabaseServer


db_server = DatabaseServer().server


class TestBank(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.client = db_server.test_client()

    def setUp(self):
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_get(self):
        """ GET on / """
        response = self.client.get('/')
        self.assertEqual(response.status_code, 418)

    def test_post(self):
        """ POST on / """
        response = self.client.post('/')
        self.assertEqual(response.status_code, 405)

    def test_put(self):
        """ PUT on / """
        response = self.client.put('/')
        self.assertEqual(response.status_code, 405)
