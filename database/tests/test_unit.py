#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
import unittest
import json

from models.base import db
from models import UnitModel
from repositories import UnitRepository
from server import DatabaseServer


db_server = DatabaseServer().server


class TestUnit(unittest.TestCase):

    data1 = {'id': 1, 'country': 'country A', 'internet_code': 'code A',
             'parent_id': None, 'name': 'Test 1',
             'source': 'Nowhere', 'symbol': 'X', 'typ': 'A'}
    data2 = {'id': 2, 'country': 'country B', 'internet_code': 'code B',
             'parent_id': None, 'name': 'Test 2',
             'source': 'Somewhere', 'symbol': 'Y', 'typ': 'B'}
    data1_no_id = {'country': 'country A', 'internet_code': 'code A',
                   'parent_id': None, 'name': 'Test 1',
                   'source': 'Nowhere', 'symbol': 'X', 'typ': 'A'}
    data2_no_id = {'country': 'country B', 'internet_code': 'code B',
                   'parent_id': None, 'name': 'Test 2',
                   'source': 'Somewhere', 'symbol': 'Y', 'typ': 'B'}

    @classmethod
    def setUpClass(cls):
        cls.client = db_server.test_client()

    def setUp(self):
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_get(self):
        """ GET on /units with ID should return a unit """
        UnitRepository.create(**self.data1_no_id)
        response = self.client.get('/units/{}'.format(self.data1['id']))

        self.assertEqual(response.status_code, 200,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json, {'unit': [self.data1, {}]})

    def test_get_fail(self):
        """ GET on /units with invalid should not return a unit """
        # Get unknown unit
        response = self.client.get('/units/1')
        self.assertEqual(response.status_code, 404,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'message': 'Unit ID 1 not found'})

    def test_get_all(self):
        """ GET on /units without ID should return all units """
        UnitRepository.create(**self.data1_no_id)
        UnitRepository.create(**self.data2_no_id)
        response = self.client.get('/units')

        self.assertEqual(response.status_code, 200,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'unit': [[self.data1, self.data2], {}]})

    def test_get_all_empty(self):
        """ GET on /units with empty unit data should return 404 """
        response = self.client.get('/units')
        self.assertEqual(response.status_code, 404,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'message': 'No units found'})

    def test_create(self):
        """ POST on /units should create a unit """
        response = self.client.post(
            '/units',
            content_type='application/json',
            data=json.dumps(self.data1_no_id)
        )

        # Verify API return
        self.assertEqual(response.status_code, 201,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'message': 'Created unit',
                          'unit': [self.data1, {}]})
        self.assertEqual(UnitModel.query.count(), 1)

        # Verify database info
        unit = UnitRepository.get(id=self.data1['id'])
        self.assertEqual(unit.country, self.data1['country'])
        self.assertEqual(unit.id, self.data1['id'])
        self.assertEqual(unit.internet_code, self.data1['internet_code'])
        self.assertEqual(unit.parent_id, self.data1['parent_id'])
        self.assertEqual(unit.name, self.data1['name'])
        self.assertEqual(unit.source, self.data1['source'])
        self.assertEqual(unit.symbol, self.data1['symbol'])
        self.assertEqual(unit.typ, self.data1['typ'])

    def test_create_fail(self):
        """ bad POST on /units should NOT create a unit """
        post_data = {'plop': 'Test'}
        response = self.client.post(
            '/units',
            content_type='application/json',
            data=json.dumps(post_data)
        )

        # Verify API return
        self.assertEqual(response.status_code, 400,
                         msg=json.loads(response.data.decode('utf-8')))

    def test_update(self):
        """ PUT on /units should update an unit's info """
        UnitRepository.create(**self.data2_no_id)
        put_data = {k: v for k, v in self.data2_no_id.items()}
        put_data['id'] = self.data1['id']
        response = self.client.put(
            '/units/{}'.format(put_data['id']),
            content_type='application/json',
            data=json.dumps(put_data)
        )

        # Verify API return
        self.assertEqual(response.status_code, 200,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'message': 'Updated unit',
                          'unit': [put_data, {}]})

        # Verify database info
        unit = UnitRepository.get(id=put_data['id'])
        self.assertEqual(unit.country, put_data['country'])
        self.assertEqual(unit.id, put_data['id'])
        self.assertEqual(unit.internet_code, put_data['internet_code'])
        self.assertEqual(unit.parent_id, put_data['parent_id'])
        self.assertEqual(unit.name, put_data['name'])
        self.assertEqual(unit.source, put_data['source'])
        self.assertEqual(unit.symbol, put_data['symbol'])
        self.assertEqual(unit.typ, put_data['typ'])

    def test_update_fail(self):
        """ PUT on /units with bad info should not update an unit """
        # Bad unit ID
        UnitRepository.create(**self.data1_no_id)
        response = self.client.put(
            '/units/{}'.format(self.data2['id']),
            content_type='application/json',
            data=json.dumps(self.data2_no_id)
        )
        self.assertEqual(response.status_code, 404,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'message': 'Unit ID 2 not found'})

        # Verify database info
        unit = UnitRepository.get(id=self.data1['id'])
        self.assertEqual(unit.country, self.data1['country'])
        self.assertEqual(unit.id, self.data1['id'])
        self.assertEqual(unit.internet_code, self.data1['internet_code'])
        self.assertEqual(unit.parent_id, self.data1['parent_id'])
        self.assertEqual(unit.name, self.data1['name'])
        self.assertEqual(unit.source, self.data1['source'])
        self.assertEqual(unit.symbol, self.data1['symbol'])
        self.assertEqual(unit.typ, self.data1['typ'])

    def test_delete(self):
        """ DEL on /units should delete a unit """
        UnitRepository.create(**self.data1_no_id)
        unit = UnitRepository.get(id=self.data1['id'])
        self.assertEqual(unit.id, self.data1['id'])

        response = self.client.delete('/units/{}'.format(self.data1['id']))

        # Verify API return
        self.assertEqual(response.status_code, 202)
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'message': 'Deleted unit',
                          'unit': [self.data1, {}]})

        # Verify database info
        unit = UnitRepository.get(id=self.data1['id'])
        self.assertEqual(unit, None)

    def test_delete_fail(self):
        """ DEL on /units should delete a unit """
        UnitRepository.create(**self.data1_no_id)
        UnitRepository.create(**self.data2_no_id)
        unit = UnitRepository.get(id=self.data1['id'])
        self.assertEqual(unit.id, self.data1['id'])

        response = self.client.delete('/units/3')

        # Verify API return
        self.assertEqual(response.status_code, 404,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'message': 'Unit ID 3 not found'})

        # Verify database info
        unit = UnitRepository.get(id=self.data1['id'])
        self.assertNotEqual(unit, None)
        unit = UnitRepository.get(id=self.data2['id'])
        self.assertNotEqual(unit, None)

    def test_repr(self):
        """ Verify the repr function of the model works """
        self.assertIsInstance(str(UnitModel(**self.data1_no_id)), str)
