#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
import unittest
import flask
import os
from socket import gaierror

from server import DatabaseServer


class TestDatabase(unittest.TestCase):
    ''' Test the database server '''

    @classmethod
    def setUpClass(cls):
        cls.db_srv = None

    def setUp(self):
        self.db_srv = DatabaseServer()
        self.db_srv.server.config['TESTING'] = True

    def test_setup(self):
        ''' Test setting up the db server '''
        self.assertIsInstance(self.db_srv.server, flask.Flask)
        self.assertTrue(self.db_srv.db_connected)

    def test_invalid_db(self):
        ''' Test connection failure '''
        self.db_uri = 'sqlite:///' + os.path.join('test.db')
        self.db_srv.server.config['TESTING'] = True
        self.db_srv.server.config['WTF_CSRF_ENABLED'] = False
        self.db_srv.server.config['SQLALCHEMY_DATABASE_URI'] = self.db_uri
        self.db_srv.connect()

    def test_start_fail(self):
        ''' Raises an exception when server can't run '''
        self.assertRaises(gaierror, self.db_srv.run)
        self.assertFalse(self.db_srv.running)

    def test_stop_fail(self):
        ''' Test connection failure '''
        self.db_srv.stop()
        self.assertFalse(self.db_srv.running)
