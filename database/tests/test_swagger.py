#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
import unittest
import json
import warnings

from server import DatabaseServer


db_server = DatabaseServer().server


class TestSwagger(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.client = db_server.test_client()

    def test_get_spec(self):
        """ The GET on /spec should return a 200 """

        with warnings.catch_warnings():
            warnings.filterwarnings("ignore", message="unclosed file")
            response = self.client.get('/spec')
        self.assertEqual(response.status_code, 200)

    def test_swagger_is_not_empty(self):
        """
        The GET on /spec should return a dict with a non-empty paths property
        """

        with warnings.catch_warnings():
            warnings.filterwarnings("ignore", message="unclosed file")
            response = self.client.get('/spec')
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertTrue(response_json['paths'])
