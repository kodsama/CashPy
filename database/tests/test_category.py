#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
import unittest
import json

from models.base import db
from models import CategoryModel
from repositories import CategoryRepository
from server import DatabaseServer


db_server = DatabaseServer().server


class TestCategory(unittest.TestCase):

    data1 = {'name': 'Test', 'icon': 'Nope', 'parent_id': None, 'id': 1}
    data2 = {'name': 'Test 2', 'icon': 'None', 'parent_id': None, 'id': 2}
    data1_no_id = {'name': 'Test', 'icon': 'Nope', 'parent_id': None}
    data2_no_id = {'name': 'Test 2', 'icon': 'None', 'parent_id': None}

    @classmethod
    def setUpClass(cls):
        cls.client = db_server.test_client()

    def setUp(self):
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_get(self):
        """ The GET on /categories with ID should return a category """
        CategoryRepository.create(name=self.data1['name'],
                                  icon=self.data1['icon'],
                                  parent_id=self.data1['parent_id'])
        response = self.client.get('/categories/{}'.format(self.data1['id']))

        self.assertEqual(response.status_code, 200,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json, {'category': [self.data1, {}]})

    def test_get_fail(self):
        """ An invalid GET on /categories should not return a category """
        response = self.client.get('/categories/1')
        self.assertEqual(response.status_code, 404,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'message': 'Category ID 1 not found'})

    def test_get_all(self):
        """ The GET on /categories without ID should return all categories """
        CategoryRepository.create(name=self.data1['name'],
                                  icon=self.data1['icon'],
                                  parent_id=self.data1['parent_id'])
        CategoryRepository.create(name=self.data2['name'],
                                  icon=self.data2['icon'],
                                  parent_id=self.data2['parent_id'])
        response = self.client.get('/categories')

        self.assertEqual(response.status_code, 200,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'category': [[self.data1, self.data2], {}]})

    def test_get_all_empty(self):
        """ The GET on /categories with no category data should return 404 """
        response = self.client.get('/categories')
        self.assertEqual(response.status_code, 404,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'message': 'No categories found'})

    def test_create(self):
        """ The POST on /categories should create a category """
        response = self.client.post(
            '/categories',
            content_type='application/json',
            data=json.dumps(self.data1_no_id)
        )

        # Verify API return
        self.assertEqual(response.status_code, 201,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'message': 'Created category',
                          'category': [self.data1, {}]})
        self.assertEqual(CategoryModel.query.count(), 1)

        # Verify database info
        category = CategoryRepository.get(id=self.data1['id'])
        self.assertEqual(category.name, self.data1['name'])
        self.assertEqual(category.icon, self.data1['icon'])
        self.assertEqual(category.parent_id, self.data1['parent_id'])

    def test_create_fail(self):
        """ The bad POST on /categories should NOT create a category """
        post_data = {'plop': 'Test'}
        response = self.client.post(
            '/categories',
            content_type='application/json',
            data=json.dumps(post_data)
        )

        # Verify API return
        self.assertEqual(response.status_code, 400)

    def test_update(self):
        """ The PUT on /categories should update an category's info """
        CategoryRepository.create(name=self.data1['name'],
                                  icon=self.data1['icon'],
                                  parent_id=self.data1['parent_id'])
        put_data = {k: v for k, v in self.data2_no_id.items()}
        put_data['id'] = self.data1['id']
        response = self.client.put(
            '/categories/{}'.format(put_data['id']),
            content_type='application/json',
            data=json.dumps(put_data)
        )

        # Verify API return
        self.assertEqual(response.status_code, 200,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'message': 'Updated category',
                          'category': [put_data, {}]})

        # Verify database info
        category = CategoryRepository.get(id=put_data['id'])
        self.assertEqual(category.id, self.data1['id'])
        self.assertEqual(category.name, self.data2['name'])
        self.assertEqual(category.icon, self.data2['icon'])
        self.assertEqual(category.parent_id, self.data2['parent_id'])

    def test_update_fail(self):
        """ The PUT on /categories with bad ID should not update a category """
        CategoryRepository.create(name=self.data1['name'],
                                  icon=self.data1['icon'],
                                  parent_id=self.data1['parent_id'])
        response = self.client.put(
            '/categories/{}'.format(self.data2['id']),
            content_type='application/json',
            data=json.dumps(self.data2_no_id)
        )

        # Verify API return
        self.assertEqual(response.status_code, 404,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'message': 'Category ID 2 not found'})

        # Verify database info
        category = CategoryRepository.get(id=self.data1['id'])
        self.assertEqual(category.name, self.data1['name'])
        self.assertEqual(category.icon, self.data1['icon'])
        self.assertEqual(category.parent_id, self.data1['parent_id'])

    def test_delete(self):
        """ The DEL on /categories should delete a category """
        CategoryRepository.create(name=self.data1['name'],
                                  icon=self.data1['icon'],
                                  parent_id=self.data1['parent_id'])
        CategoryRepository.create(name=self.data2['name'],
                                  icon=self.data2['icon'],
                                  parent_id=self.data2['parent_id'])
        category = CategoryRepository.get(id=self.data1['id'])
        self.assertEqual(category.id, self.data1['id'])

        response = self.client.delete('/categories/{}'.format(
            self.data1['id']))

        # Verify API return
        self.assertEqual(response.status_code, 202,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'message': 'Deleted category',
                          'category': [self.data1, {}]})

        # Verify database info
        category = CategoryRepository.get(id=self.data1['id'])
        self.assertEqual(category, None)

    def test_delete_fail(self):
        """ The DEL on /categories should delete a category """
        CategoryRepository.create(name=self.data1['name'],
                                  icon=self.data1['icon'],
                                  parent_id=self.data1['parent_id'])
        CategoryRepository.create(name=self.data2['name'],
                                  icon=self.data2['icon'],
                                  parent_id=self.data2['parent_id'])
        category = CategoryRepository.get(id=self.data1['id'])
        self.assertEqual(category.id, self.data1['id'])

        response = self.client.delete('/categories/3')

        # Verify API return
        self.assertEqual(response.status_code, 404,
                         msg=json.loads(response.data.decode('utf-8')))
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response_json,
                         {'message': 'Category ID 3 not found'})

        # Verify database info
        category = CategoryRepository.get(id=self.data1['id'])
        self.assertNotEqual(category, None)
        category = CategoryRepository.get(id=self.data2['id'])
        self.assertNotEqual(category, None)

    def test_repr(self):
        """ Verify the repr function of the model works """
        self.assertIsInstance(str(CategoryModel(**self.data1_no_id)), str)
