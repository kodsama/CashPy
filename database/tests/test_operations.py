#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CashPy
Copyright 2018 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
"""
import unittest
import json

from models.base import db
from models import OperationModel
from repositories import OperationRepository
from repositories import (
    BankRepository,
    AccountRepository,
    CategoryRepository,
    PayeeRepository
)
from server import DatabaseServer


db_server = DatabaseServer().server


class TestOperation(unittest.TestCase):

    bank_data1_no_id = {'name': 'Test'}
    account_data1_no_id = {'bank_id': 1, 'name': 'Acc 1', 'opened': True,
                           'highlighted': False, 'typ': 'Card'}
    account_data2_no_id = {'bank_id': 1, 'name': 'Acc 2', 'opened': True,
                           'highlighted': False, 'typ': 'Card'}
    category_data1_no_id = {'name': 'Test 1', 'parent_id': None}
    category_data2_no_id = {'name': 'Test 2', 'parent_id': None}
    payee_data1_no_id = {'name': 'Joe Doe'}
    payee_data2_no_id = {'name': 'Joe Maverick'}

    data1 = {'id': 1, 'account_id': 1, 'amount': 10,
             'category_id': 1, 'comment': 'Nowhere',
             'date': '2001-01-01', 'parent_id': None, 'payee_id': 1,
             'validated': True}
    data2 = {'id': 2, 'account_id': 2, 'amount': 20,
             'category_id': 2, 'comment': 'Somewhere',
             'date': '2002-02-02', 'parent_id': None, 'payee_id': 2,
             'validated': False}
    data1_no_id = {'account_id': 1, 'amount': 10,
                   'category_id': 1, 'comment': 'Nowhere',
                   'date': '2001-01-01', 'parent_id': None,
                   'payee_id': 1, 'validated': True}
    data2_no_id = {'account_id': 2, 'amount': 20,
                   'category_id': 2, 'comment': 'Somewhere',
                   'date': '2002-02-02', 'parent_id': None,
                   'payee_id': 2, 'validated': False}

    @classmethod
    def setUpClass(cls):
        cls.client = db_server.test_client()

    def setUp(self):
        db.create_all()
        # Create all dependant tables
        BankRepository.create(**self.bank_data1_no_id)
        AccountRepository.create(**self.account_data1_no_id)
        AccountRepository.create(**self.account_data2_no_id)
        CategoryRepository.create(**self.category_data1_no_id)
        CategoryRepository.create(**self.category_data2_no_id)
        PayeeRepository.create(**self.payee_data1_no_id)
        PayeeRepository.create(**self.payee_data2_no_id)

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_get(self):
        """ GET on /operations with ID should return an operation """
        OperationRepository.create(**self.data1_no_id)
        response = self.client.get('/operations/{}'.format(self.data1['id']))

        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response.status_code, 200, response_json)
        self.assertEqual(response_json, {'operation': [self.data1, {}]})

    def test_get_fail(self):
        """
        GET on /operations with invalid should not return an operation
        """
        # Get unknown operation
        response = self.client.get('/operations/1')
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response.status_code, 404, response_json)
        self.assertEqual(response_json,
                         {'message': 'Operation ID 1 not found'})

    def test_get_all(self):
        """ GET on /operations without ID should return all operations """
        OperationRepository.create(**self.data1_no_id)
        OperationRepository.create(**self.data2_no_id)
        response = self.client.get('/operations')

        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response.status_code, 200, response_json)
        print(response_json)
        self.assertEqual(response_json,
                         {'operation': [[self.data1, self.data2], {}]})

    def test_get_all_empty(self):
        """ GET on /operations with no operation data should return 404 """
        response = self.client.get('/operations')
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response.status_code, 404, response_json)
        self.assertEqual(response_json,
                         {'message': 'No operations found'})

    def test_create(self):
        """ POST on /operations should create an operation """
        BankRepository.create(**self.bank_data1_no_id)
        response = self.client.post(
            '/operations',
            content_type='application/json',
            data=json.dumps(self.data1_no_id)
        )

        # Verify API return
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response.status_code, 201, response_json)
        self.assertEqual(response_json,
                         {'message': 'Created operation',
                          'operation': [self.data1, {}]})
        self.assertEqual(OperationModel.query.count(), 1)

        # Verify database info
        operation = OperationRepository.get(id=self.data1['id'])
        self.assertEqual(operation.id, self.data1['id'])
        self.assertEqual(operation.account_id, self.data1['account_id'])
        self.assertEqual(operation.amount, self.data1['amount'])
        self.assertEqual(operation.category_id, self.data1['category_id'])
        self.assertEqual(operation.comment, self.data1['comment'])
        self.assertEqual(operation.date.strftime('%Y-%m-%d'),
                         self.data1['date'])
        self.assertEqual(operation.parent_id, self.data1['parent_id'])
        self.assertEqual(operation.payee_id, self.data1['payee_id'])
        self.assertEqual(operation.validated, self.data1['validated'])

    def test_create_fail(self):
        """ bad POST on /operations should NOT create an operation """
        post_data = {'plop': 'Test'}
        response = self.client.post(
            '/operations',
            content_type='application/json',
            data=json.dumps(post_data)
        )

        # Verify API return
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response.status_code, 400, response_json)

    def test_update(self):
        """ PUT on /operations should update an operation's info """
        OperationRepository.create(**self.data2_no_id)
        put_data = {k: v for k, v in self.data2_no_id.items()}
        put_data['id'] = self.data1['id']
        response = self.client.put(
            '/operations/{}'.format(put_data['id']),
            content_type='application/json',
            data=json.dumps(put_data)
        )

        # Verify API return
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response.status_code, 200, response_json)
        self.assertEqual(response_json,
                         {'message': 'Updated operation',
                          'operation': [put_data, {}]})

        # Verify database info
        operation = OperationRepository.get(id=put_data['id'])
        self.assertEqual(operation.id, put_data['id'])
        self.assertEqual(operation.account_id, put_data['account_id'])
        self.assertEqual(operation.amount, put_data['amount'])
        self.assertEqual(operation.category_id, put_data['category_id'])
        self.assertEqual(operation.comment, put_data['comment'])
        self.assertEqual(operation.date.strftime('%Y-%m-%d'),
                         put_data['date'])
        self.assertEqual(operation.parent_id, put_data['parent_id'])
        self.assertEqual(operation.payee_id, put_data['payee_id'])
        self.assertEqual(operation.validated, put_data['validated'])

    def test_update_fail(self):
        """ PUT on /operations with bad info should not update """
        # Bad operation ID
        BankRepository.create(**self.bank_data1_no_id)
        OperationRepository.create(**self.data1_no_id)
        response = self.client.put(
            '/operations/{}'.format(self.data2['id']),
            content_type='application/json',
            data=json.dumps(self.data2_no_id)
        )
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response.status_code, 404, response_json)
        self.assertEqual(response_json,
                         {'message': 'Operation ID 2 not found'})

        # Verify database info
        operation = OperationRepository.get(id=self.data1['id'])
        self.assertEqual(operation.id, self.data1['id'])
        self.assertEqual(operation.account_id, self.data1['account_id'])
        self.assertEqual(operation.amount, self.data1['amount'])
        self.assertEqual(operation.category_id, self.data1['category_id'])
        self.assertEqual(operation.comment, self.data1['comment'])
        self.assertEqual(operation.date.strftime('%Y-%m-%d'),
                         self.data1['date'])
        self.assertEqual(operation.parent_id, self.data1['parent_id'])
        self.assertEqual(operation.payee_id, self.data1['payee_id'])
        self.assertEqual(operation.validated, self.data1['validated'])

    def test_delete(self):
        """ DEL on /operations should delete an operation """
        OperationRepository.create(**self.data1_no_id)
        operation = OperationRepository.get(id=self.data1['id'])
        self.assertEqual(operation.id, self.data1['id'])

        response = self.client.delete('/operations/{}'.format(
                                      self.data1['id']))

        # Verify API return
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response.status_code, 202, response_json)
        self.assertEqual(response_json,
                         {'message': 'Deleted operation',
                          'operation': [self.data1, {}]})

        # Verify database info
        operation = OperationRepository.get(id=self.data1['id'])
        self.assertEqual(operation, None)

    def test_delete_fail(self):
        """ DEL on /operations should delete an operation """
        OperationRepository.create(**self.data1_no_id)
        OperationRepository.create(**self.data2_no_id)
        operation = OperationRepository.get(id=self.data1['id'])
        self.assertEqual(operation.id, self.data1['id'])

        response = self.client.delete('/operations/3')

        # Verify API return
        response_json = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response.status_code, 404, response_json)
        self.assertEqual(response_json,
                         {'message': 'Operation ID 3 not found'})

        # Verify database info
        operation = OperationRepository.get(id=self.data1['id'])
        self.assertNotEqual(operation, None)
        operation = OperationRepository.get(id=self.data2['id'])
        self.assertNotEqual(operation, None)

    def test_repr(self):
        """ Verify the repr function of the model works """
        self.assertIsInstance(str(OperationModel(**self.data1_no_id)), str)
