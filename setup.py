#!/usr/bin/env python3
# Copyright 2017 Alexandre Martins (a.k.a Maral Afris). All rights reserved.
import sys
from setuptools import setup, find_packages

if sys.version_info[0] < 3:
    sys.exit("Sorry, Python 2 is not supported")

__version__ = '0.0.1'

NAME = 'CashPy'

DESCRIPTION = """
Yet another accounting software
"""

PACKAGES = find_packages(exclude=['test', 'test_*.*'])

setup(
    name=NAME,
    version=__version__,
    packages=PACKAGES,
    install_requires=[],
    extras_require={
        'dev': [
            'pytest>=3.6.3',
            'coverage>=4.5.1'
            ]
        },
    description=DESCRIPTION,
    long_description=DESCRIPTION,
    package_data={
        '': ['*.md']
        },
    author="Alexandre martins (a.k.a Maral Afris)",
    author_email="maral.afris@gmail.com",
    license="Proprietary",
    keywords="cash accounting",
    url="https://gitlab.com/MaralAfris/CashPy",
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Build Tools',
        'Environment :: Other Environment',
        'Operating System :: OS Independent',
        'License :: Other/Proprietary License',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.2',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Topic :: Utilities',
    ],
    zip_safe=True
)
