# CashPy

## Yet another accounting software
[![build status](https://gitlab.com/MaralAfris/CashMe/badges/master/build.svg)](https://gitlab.com/MaralAfris/CashMe/commits/master)
[![coverage report](https://gitlab.com/MaralAfris/CashMe/badges/master/coverage.svg)](https://gitlab.com/MaralAfris/CashMe/commits/master)

## Architecture
The software is separated in two parts: a server and a client
Each part has their own database.

## Databases
The server database contains common data that each client uses and updates.
In this case the system is expanded and maintained by the users while details
stay only available to clients.

The server database contains information like:
    - Banks
    - Stocks and their levels
    - Currencies
    - Categories
    - Payees

The clients contain:
    - Accounts
    - Transactions
    - Stocks owned
    - Private Categories
    - Private payees

## Requirements
This software requires:
docker
docker-compose
python3

## Installation
Just run:
```
make install
```

## Usage
Run:
```
make build
```

Then go to:
http://localhost:3000

Or to check out/test the api:
http://localhost:3000/apidocs/

Run tests:
```
make tests
```

Run linting:
```
make lint
```
